import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.ticker import MaxNLocator

class PlotWiFiFtmLocalizationExp2:

    def __init__(self, experiment_id, local_path):
        self.experiment_id = experiment_id
        self.local_path = local_path

    def plot_all_metrics(self):
        # merge_all_location_diff
        df3 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(3) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')
        df3['dist_group'] = 3

        df5 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(5) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')
        df5['dist_group'] = 5

        df7 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(7) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')
        df7['dist_group'] = 7

        df10 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(10) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')
        df10['dist_group'] = 10

        df15 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(15) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')
        df15['dist_group'] = 15

        df = pd.concat([df3, df5, df7, df10, df15])

        std_gps = df['dist_DD_GT_latlong_est_latlong'].std()
        std_dist = df['HD_DD_dist_diffs'].std()
        std_bearing = df['HD_DD_bearing_diffs'].std()

        print("std gps error: ", self.experiment_id, std_gps)
        print("mean gps error: ", self.experiment_id, df['dist_DD_GT_latlong_est_latlong'].mean())

        print("std dist error: ", self.experiment_id, std_dist)
        print("mean dist error: ", self.experiment_id, df['HD_DD_dist_diffs'].mean())

        print("std bearing error: ", self.experiment_id, std_bearing)
        print("mean bearing error: ", self.experiment_id, df['HD_DD_bearing_diffs'].mean())

        sns.boxplot(x='dist_group', y='dist_DD_GT_latlong_est_latlong', data=df)
        plt.xlabel("Distance(m)", size=24)
        plt.ylabel("Error(m)", size=24)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.grid(color='gray', linestyle='--', linewidth=0.5)
        plt.tight_layout()
        # plt.rcParams['font.size'] = '16'
        # plt.rcParams.update({'figure.autolayout': True})

        # plt.title("Estimated lat-long error compared with ground truth lat-long", size=12)
        plt.savefig('data/plot/' + self.experiment_id + '/dist_DD_GT_latlong_est_latlong.png')
        # plt.savefig('data/plot/dist_DD_GT_latlong_est_latlong.eps')
        plt.clf()

        sns.boxplot(x='dist_group', y='HD_DD_bearing_diffs', data=df)
        plt.xlabel("Distance(m)", size=24)
        plt.ylabel("Error(deg)", size=24)

        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.grid(color='gray', linestyle='--', linewidth=0.5)
        plt.tight_layout()
        # plt.title("Estimated bearing error compared with ground truth bearing", size=12)
        plt.savefig('data/plot/' + self.experiment_id + '/HD_DD_bearing_diffs.png')
        # plt.savefig('data/plot/HD_DD_bearing_diffs.eps')

        plt.clf()

        self.HD_DD_dist_compare(df3, 3)
        self.HD_DD_dist_compare(df5, 5)
        self.HD_DD_dist_compare(df7, 7)
        self.HD_DD_dist_compare(df10, 10)
        self.HD_DD_dist_compare(df15, 15)

        self.HD_DD_dist_compare_with_error(df3, 3)
        self.HD_DD_dist_compare_with_error(df5, 5)
        self.HD_DD_dist_compare_with_error(df7, 7)
        self.HD_DD_dist_compare_with_error(df10, 10)
        self.HD_DD_dist_compare_with_error(df15, 15)

        self.HD_DD_dist_compare_with_error_subplot()
        self.HD_DD_dist_GPS_error_subplot()

    def HD_DD_dist_compare(self, df, triangle_height):
        # df.insert(0, 'id', range(1, 1 + len(df)))

        df_new = pd.DataFrame(df, columns=['GT_side_HD_DD', 'side_HD_DD'])
        sns.lineplot(data=df_new)
        plt.xlabel("Time")
        plt.ylabel("Distance(m)")
        plt.title("HD to DD estimated distance compare to ground truth of " + str(triangle_height) + " meter",
                  size=8)
        plt.legend(labels=["Ground Truth", "Estimated"])
        plt.plot()
        plt.savefig(
            'data/plot/' + self.experiment_id + '/HD_DD_distance_compare_auto_inc' + str(triangle_height) + '.png')
        plt.clf()

    def HD_DD_dist_compare_with_error(self, df, triangle_height):
        df.insert(0, 'id', range(1, 1 + len(df)))

        x = df['id']
        y = df['GT_side_HD_DD']
        plt.errorbar(x, y, yerr=None, label='Line1')
        z = df['side_HD_DD']
        error = df['HD_DD_dist_diffs'].std()

        plt.errorbar(x, z, yerr=error, c='g', label='Line2', capsize=2, capthick=1, errorevery=3)

        plt.ylim([0, max(z) + 5])
        plt.xlabel("Time")
        plt.ylabel("Distance(m)")
        plt.title(
            "HD to DD estimated distance compare to ground truth of " + str(triangle_height) + " meter with error",
            size=8)
        plt.legend(labels=["Ground Truth", "Estimated"])
        plt.plot()
        plt.savefig(
            'data/plot/' + self.experiment_id + '/HD_DD_distance_compare_error' + str(triangle_height) + '.png')
        plt.clf()

    def HD_DD_dist_compare_with_error_subplot(self):
        fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, sharex=False)
        fig.suptitle('HD to DD estimated distance comparison with error')
        # Set common labels
        fig.text(0.5, 0.04, 'Time', ha='center', va='center')
        fig.text(0.06, 0.5, 'Distance(m)', ha='center', va='center', rotation='vertical')
        for ax in (ax1, ax2, ax3, ax4, ax5):
            ax.set_xticks([])

        df3 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(3) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')

        df3.insert(0, 'id', range(1, 1 + len(df3)))

        x = df3['id']
        y = df3['GT_side_HD_DD']
        ax1.errorbar(x, y, yerr=None, label='Line1', c='r')
        z = df3['side_HD_DD']
        error = df3['HD_DD_dist_diffs'].std()

        ax1.errorbar(x, z, yerr=error, c='g', label='Line2', capsize=2, capthick=1, errorevery=3)
        ax1.set_xlabel('3 meter', size=8)
        ax1.yaxis.set_major_locator(MaxNLocator(integer=True, nbins=3))
        ax1.xaxis.set_label_position("top")
        # ax1.yaxis.set_ticks(np.arange(0, max(z)+1, 1))
        # ax1.set_ylim([0, max(z)+1])

        l1 = ax1.plot()

        df5 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(5) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')

        df5.insert(0, 'id', range(1, 1 + len(df5)))

        x = df5['id']
        y = df5['GT_side_HD_DD']
        ax2.errorbar(x, y, yerr=None, label='Line1', c='r')
        z = df5['side_HD_DD']
        error = df5['HD_DD_dist_diffs'].std()
        ax2.yaxis.set_major_locator(MaxNLocator(integer=True, nbins=3))
        ax2.set_xlabel('5 meter', size=8)
        ax2.xaxis.set_label_position("top")
        # ax2.set_ylim([0, max(z)+1])
        # ax2.yaxis.set_ticks(np.arange(0, max(z)+1, 1))

        ax2.errorbar(x, z, yerr=error, c='g', label='Line2', capsize=2, capthick=1, errorevery=3)
        l2 = ax2.plot()

        df7 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(7) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')

        df7.insert(0, 'id', range(1, 1 + len(df7)))

        x = df7['id']
        y = df7['GT_side_HD_DD']
        ax3.errorbar(x, y, yerr=None, label='Line3', c='r')
        z = df7['side_HD_DD']
        error = df7['HD_DD_dist_diffs'].std()
        ax3.yaxis.set_major_locator(MaxNLocator(integer=True, nbins=3))
        ax3.set_xlabel('7 meter', size=8)
        ax3.xaxis.set_label_position("top")

        # ax3.set_ylim([0, max(z)+1])

        ax3.errorbar(x, z, yerr=error, c='g', label='Line3', capsize=2, capthick=1, errorevery=3)
        l3 = ax3.plot()

        df10 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(10) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')

        df10.insert(0, 'id', range(1, 1 + len(df10)))

        x = df10['id']
        y = df10['GT_side_HD_DD']
        ax4.errorbar(x, y, yerr=None, label='Line3', c='r')
        z = df10['side_HD_DD']
        error = df10['HD_DD_dist_diffs'].std()
        ax4.set_xlabel('10 meter', size=8)
        # ax4.set_ylim([0, max(z)+1])
        # ax2.yaxis.set_ticks(np.arange(0, max(z)+1, 1))
        ax4.yaxis.set_major_locator(MaxNLocator(integer=True, nbins=3))
        ax4.xaxis.set_label_position("top")

        ax4.errorbar(x, z, yerr=error, c='g', label='Line3', capsize=2, capthick=1, errorevery=3)
        l4 = ax4.plot()

        df15 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(15) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')

        df15.insert(0, 'id', range(1, 1 + len(df15)))

        x = df15['id']
        y = df15['GT_side_HD_DD']
        ax5.errorbar(x, y, yerr=None, label='Line3', c='r')
        z = df15['side_HD_DD']
        error = df15['HD_DD_dist_diffs'].std()
        ax5.yaxis.set_major_locator(MaxNLocator(integer=True, nbins=3))
        ax5.xaxis.set_label_position("top")
        ax5.set_xlabel('15 meter', size=8)
        # ax5.set_ylim([0, max(z)+1])

        ax5.errorbar(x, z, yerr=error, c='g', label='Line3', capsize=2, capthick=1, errorevery=3)
        l5 = ax5.plot()

        # Labels to use in the legend for each line
        line_labels = ["Ground Truth", "Estimated"]

        # Create the legend
        fig.legend([l1, l2],  # The line objects
                   labels=line_labels,  # The labels for each line
                   loc="lower right",  # Position of legend
                   borderaxespad=0.1,  # Small spacing around legend box
                   # title="Legend Title"  # Title for the legend
                   )

        # Adjust the scaling factor to fit your legend text completely outside the plot
        # (smaller value results in more space being made for the legend)
        plt.subplots_adjust(right=0.85, wspace = None, hspace = 0.3)

        plt.savefig('data/plot/' + self.experiment_id + '/HD_DD_distance_compare_error_subplot.png')
        plt.clf()

    def HD_DD_dist_GPS_error_subplot(self):
        fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, sharex=False)
        # plt.tight_layout()
        # fig.suptitle('HD to DD estimated distance comparison with error')
        # Set common labels
        fig.text(0.5, 0.04, 'Time', ha='center', va='center', size=24)
        fig.text(0.06, 0.5, 'Distance(m)', ha='center', va='center', rotation='vertical', size=24)
        for ax in (ax1, ax2, ax3, ax4, ax5):
            ax.set_xticks([])

        df3 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(3) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')

        df3.insert(0, 'id', range(1, 1 + len(df3)))

        x = df3['id']

        dist_diff = df3['HD_DD_dist_diffs']
        ax1.set_xlabel('3 meter', size=18, loc='right')
        ax1.yaxis.set_major_locator(MaxNLocator(integer=True, nbins=3))
        ax1.xaxis.set_label_position("bottom")
        ax1.tick_params(axis='y', labelsize=18)
        # ax1.yaxis.set_ticks(np.arange(0, max(z)+1, 1))
        # ax1.set_ylim([0, max(z)+1])

        ax1.plot(x, dist_diff, label="Distance error", c='r')
        ax1.grid(color='green', linestyle='--', linewidth=0.5)
        l1 = ax1.plot()

        df5 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(5) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')

        df5.insert(0, 'id', range(1, 1 + len(df5)))

        x = df5['id']
        ax2.yaxis.set_major_locator(MaxNLocator(integer=True, nbins=3))
        ax2.set_xlabel('5 meter', size=18, loc='right')
        ax2.xaxis.set_label_position("bottom")
        ax2.tick_params(axis='y', labelsize=18)
        # ax2.set_ylim([0, max(z)+1])
        # ax2.yaxis.set_ticks(np.arange(0, max(z)+1, 1))

        dist_diff = df5['HD_DD_dist_diffs']
        ax2.plot(x, dist_diff, label="Distance error", c='r')
        ax2.grid(color='green', linestyle='--', linewidth=0.5)

        l2 = ax2.plot()

        df7 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(7) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')

        df7.insert(0, 'id', range(1, 1 + len(df7)))

        x = df7['id']
        ax3.yaxis.set_major_locator(MaxNLocator(integer=True, nbins=3))
        ax3.set_xlabel('7 meter', size=18, loc='right')
        ax3.xaxis.set_label_position("bottom")
        ax3.tick_params(axis='y', labelsize=18)
        # ax3.set_ylim([0, max(z)+1])

        dist_diff = df7['HD_DD_dist_diffs']
        ax3.plot(x, dist_diff, label="Distance error", c='r')
        ax3.grid(color='green', linestyle='--', linewidth=0.5)

        l3 = ax3.plot()

        df10 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(10) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')

        df10.insert(0, 'id', range(1, 1 + len(df10)))

        x = df10['id']
        ax4.set_xlabel('10 meter', size=18, loc='right')
        ax4.tick_params(axis='y', labelsize=18)
        ax4.yaxis.set_major_locator(MaxNLocator(integer=True, nbins=3))
        ax4.xaxis.set_label_position("bottom")
        # ax4.set_ylim([0, max(z)+1])
        # ax2.yaxis.set_ticks(np.arange(0, max(z)+1, 1))

        dist_diff = df10['HD_DD_dist_diffs']
        ax4.plot(x, dist_diff, label="Distance error", c='r')
        ax4.grid(color='green', linestyle='--', linewidth=0.5)

        l4 = ax4.plot()

        df15 = pd.read_csv(
            self.local_path + '/' + ('final_' + str(15) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')

        df15.insert(0, 'id', range(1, 1 + len(df15)))

        x = df15['id']
        ax5.yaxis.set_major_locator(MaxNLocator(integer=True, nbins=3))
        ax5.xaxis.set_label_position("bottom")
        ax5.set_xlabel('15 meter', size=18, loc='right')
        ax5.tick_params(axis='y', labelsize=18)
        # ax5.set_ylim([0, max(z)+1])

        dist_diff = df15['HD_DD_dist_diffs']
        ax5.plot(x, dist_diff, label="Distance error", c='r')
        ax5.grid(color='green', linestyle='--', linewidth=0.5)

        l5 = ax5.plot()

        # Labels to use in the legend for each line
        # line_labels = ["Ground Truth", "Estimated"]

        # Create the legend
        # fig.legend([l1, l2],  # The line objects
        #            labels=line_labels,  # The labels for each line
        #            loc="lower right",  # Position of legend
        #            borderaxespad=0.1,  # Small spacing around legend box
        #            # title="Legend Title"  # Title for the legend
        #            )

        # Adjust the scaling factor to fit your legend text completely outside the plot
        # (smaller value results in more space being made for the legend)
        # plt.subplots_adjust(right=None, wspace=None, hspace=0.4)
        if self.experiment_id == 'exp2':
            plt.subplots_adjust(left=0.14, right=0.99, top=1, bottom=0.1, hspace=0.5)
        else:
            plt.subplots_adjust(left=0.14, right=0.99, top=.99, bottom=0.1, hspace=0.5)
        # plt.subplots_adjust(right=None, wspace=None, hspace=None)

        plt.savefig('data/plot/' + self.experiment_id + '/HD_DD_distance_gps_error_subplot.png')
        plt.clf()

def __main():
    experiment_id = 'exp2'
    local_path = "data/experiment2"

    plot = PlotWiFiFtmLocalizationExp2(experiment_id, local_path)

    plot.plot_all_metrics()

    experiment_id = 'exp3'
    local_path = "data/experiment3"

    plot = PlotWiFiFtmLocalizationExp2(experiment_id, local_path)

    plot.plot_all_metrics()

if __name__ == '__main__':
    __main()
