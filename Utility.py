from LatLonUtil import *

class Utility:

    @staticmethod
    def find_diff(estimated, GT_value):
        return abs(estimated - GT_value)

    @staticmethod
    def find_bearing_diff(estimated, GT_value):

        if estimated is None or GT_value is None:
            return -1

        if type(estimated) == str and not LatLonUtil.is_number_string(estimated):
            return -1
        if type(GT_value) == str and not LatLonUtil.is_number_string(GT_value):
            return -1

        estimated_num = float(estimated)
        GT_num = float(GT_value)

        if estimated_num > GT_num:
            exterior_angle = abs(estimated_num - 360) + GT_num
        else:
            exterior_angle = abs(GT_num - 360) + estimated_num

        interior_angle = abs(GT_num - estimated_num)
        if exterior_angle < interior_angle:
            return exterior_angle
        else:
            return interior_angle

def __main():
    print()
if __name__ == '__main__':
    __main()