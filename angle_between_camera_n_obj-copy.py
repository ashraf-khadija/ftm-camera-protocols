# import numpy as np
# import math
#
# # Windows webcam camera intrinsic parameters with skew and distortion coefficients, K
# K = np.array([[3414.9, -5.0, 2078.5],
#               [0, 3423.6, 1117.6],
#               [0,    0,      1]])
#
# # Zed left camera-HD ############
# K = np.array([[529.74, 0, 643.035],
#               [0, 529.285, 356.1845],
#               [0,    0,      1]])
#
# # Perpendicular distance 45 inch, left side 45.4, right111111 1side 45.5
# # GT angle 16 degree
# x1 = 824; y1 = 578
# x2 = 1093; y2 = 574
# # Zed left camera-HD #############
#
# # Given x,y
# # x1 = 740; y1 = 680
# # x2 = 2800; y2 = 640
#
# # # 40.5 inch Straight-Horizontal
# # # Ground truth distance 40.5 inch
# # # Ground Truth angle 15.6 degree
# # x1 = 1691; y1 = 1056
# # x2 = 2668; y2 = 1044
#
# # 40.5 inch Left-Horizontal
# # Ground truth distance perpendicular : 40.5 inch, Side1 : 45.1, Side2: 41.2
# # Ground Truth angle 15.6 degree,
# # Estimated angle 15.3
# # x1 = 244; y1 = 900
# # x2 = 1293; y2 = 900
#
# # 60 inch Straight-Horizontal
# #x1 = 1814; y1 = 1006
# #x2 = 2470; y2 = 995
#
# #  3 meter straight horizontal
# # Ground truth distance perpendicular : 3 meter, Side1 :  10.10 ft, Side2: 3.2 meter
# # Ground Truth angle 46.7 degree,
# # Estimated angle 49.55
# # x1 = 754; y1 = 960
# # x2 = 3926; y2 = 927
#
# #  5 meter straight horizontal
# # Ground truth distance perpendicular : 5 meter, Side1 :  5.24 meter, Side2: 5.21 meter
# # Ground Truth angle 35.5 degree,
# # Estimated 30.95
# # x1 = 937; y1 = 1067
# # x2 = 2834; y2 = 1062
#
# #  7 meter straight horizontal
# # Ground Truth angle 25.5 degree,
# # Estimated 22.14
# # x1 = 1116; y1 = 1190
# # x2 = 2462; y2 = 1183
#
# Ki = np.linalg.inv(K)
#
# vector_1 = Ki.dot([x1, y1, 1.0])
# vector_2 = Ki.dot([x2, y2, 1.0])
#
# print("r1: ",vector_1)
# print("r2: ",vector_2)
#
# unit_vector_1 = vector_1 / np.linalg.norm(vector_1)
#
# unit_vector_2 = vector_2 / np.linalg.norm(vector_2)
#
# dot_product = np.dot(unit_vector_1, unit_vector_2)
#
# angle = np.arccos(dot_product)
# print("angle_radian : ",angle)
# print("angle_degree : ",math.degrees(angle))
#
#
#

from ProcessGpsAndImageData import *

process_data = ProcessGpsAndImageData()
local_path = "data\experiment1"
path = 'C:\khadija.csedu\MCPS\summer 2021\experiment-data\Organized Data\Experiment 1'
gps_df1 = process_data.clean_gps_data(path, 'helperLocationTest3.csv', local_path)
