import pandas as pd
from LatLonUtil import *
import matplotlib.pyplot as plt
from Utility import *

class EvaluationWiFiFtmLocalization:
    def __init__(self, experiment_id, local_path):
        self.experiment_id = experiment_id
        self.local_path = local_path

    # @staticmethod
    # def find_diff(estimated, GT_value):
    #     return abs(estimated - GT_value)

    # @staticmethod
    # def find_bearing_diff(estimated, GT_value):
    #
    #     if estimated is None or GT_value is None:
    #         return -1
    #
    #     if type(estimated) == str and not LatLonUtil.is_number_string(estimated):
    #         return -1
    #     if type(GT_value) == str and not LatLonUtil.is_number_string(GT_value):
    #         return -1
    #
    #     estimated_num = float(estimated)
    #     GT_num = float(GT_value)
    #
    #     if estimated_num > GT_num:
    #         exterior_angle = abs(estimated_num - 360) + GT_num
    #     else:
    #         exterior_angle = abs(GT_num - 360) + estimated_num
    #
    #     interior_angle = abs(GT_num - estimated_num)
    #     if exterior_angle < interior_angle:
    #         return exterior_angle
    #     else:
    #         return interior_angle

    def accuracy_measurement(self, triangle_height):
        df = pd.read_csv(self.local_path + '/' + ('final_' + str(triangle_height) + '_m_wifi_'+self.experiment_id+'.csv'),
                         float_precision='round_trip')

        # Estimated location accuracy
        dist_DD_GT_latlong_est_latlong = [LatLonUtil.dist(lat1, long1, lat2, long2)
                                          for (lat1, long1, lat2, long2) in
                                          zip(df['DD_latest_lat'], df['DD_latest_long'],
                                              df['DD_GT_lat'], df['DD_GT_long'])]

        df[['dist_DD_GT_latlong_est_latlong']] = pd.DataFrame(dist_DD_GT_latlong_est_latlong)


        # Estimated WiFiFTM-distance accuracy
        HD_pseudoHD_dist_diffs = [Utility.find_diff(estimated_dist, GT_dist)
                                        for (estimated_dist, GT_dist) in
                                            zip(df['side_HD_pseudoHD'], df['GT_side_HD_pseudoHD'])]

        df[['HD_pseudoHD_dist_diffs']] = pd.DataFrame(HD_pseudoHD_dist_diffs)

        HD_DD_dist_diffs = [Utility.find_diff(estimated_dist, GT_dist)
                                  for (estimated_dist, GT_dist) in
                                  zip(df['side_HD_DD'], df['GT_side_HD_DD'])]

        df[['HD_DD_dist_diffs']] = pd.DataFrame(HD_DD_dist_diffs)

        # Estimated bearing accuracy
        HD_DD_bearing_diffs = [Utility.find_bearing_diff(est_bearing, GT_bearing)
                            for (est_bearing, GT_bearing) in
                            zip(df['HD_DD_est_bearing'], df['HD_DD_GT_bearing'])]

        df[['HD_DD_bearing_diffs']] = pd.DataFrame(HD_DD_bearing_diffs)

        df.to_csv(self.local_path + '/' + ('final_' + str(triangle_height) + '_m_wifi_'+self.experiment_id+'.csv'), index=False)

def __main():

    experiment_id = 'exp2'
    local_path = "data/experiment2"

    evaluation = EvaluationWiFiFtmLocalization(experiment_id, local_path)

    triangle_height = 3
    evaluation.accuracy_measurement(triangle_height)

    triangle_height = 5
    evaluation.accuracy_measurement(triangle_height)

    triangle_height = 7
    evaluation.accuracy_measurement(triangle_height)

    triangle_height = 10
    evaluation.accuracy_measurement(triangle_height)

    triangle_height = 15
    evaluation.accuracy_measurement(triangle_height)

if __name__ == '__main__':
    __main()
