from ProcessGpsAndImageData import *
import pandas as pd
from PropertyReadWriteUtils import *
import math
from LatLonUtil import *


class ProcessWiFiFtmData:

    def __init__(self, experiment_id, raw_data_path, local_path):
        self.experiment_id = experiment_id
        self.raw_data_path = raw_data_path
        self.local_path = local_path

    class Triangle:
        def __init__(self):
            self.height = 0.0
            self.side_HD_pseudoHD = 0.0
            self.side_HD_DD = 0.0
            self.side_pseudoHD_DD = 0.0
            self.HD_GT_lat_long = None
            self.pseudoHD_GT_lat_long = None
            self.DD_GT_lat_long = None
            self.DD_prev_lat_long = None
            self.HD_pseudoHD_GT_bearing = None
            self.HD_DD_GT_bearing = None

    def load_gt_triangle(self, height):

        triangle = self.Triangle()

        side = PropertyReadWriteUtils.read_value_by_key(
            self.experiment_id + ".triangle.height." + str(height) + "m.side_HD_pseudoHD")
        triangle.side_HD_pseudoHD = self.inch_to_meter(self.feet_inch_to_inch(side))

        side = PropertyReadWriteUtils.read_value_by_key(
            self.experiment_id + ".triangle.height." + str(height) + "m.side_HD_DD")
        triangle.side_HD_DD = self.inch_to_meter(self.feet_inch_to_inch(side))

        side_pseudoHD_DD_unit = PropertyReadWriteUtils.read_value_by_key(
            self.experiment_id + ".triangle.height." + str(height) + "m.side_pseudoHD_DD_unit")

        side = PropertyReadWriteUtils.read_value_by_key(
            self.experiment_id + ".triangle.height." + str(height) + "m.side_pseudoHD_DD")

        if side_pseudoHD_DD_unit and "meter" in side_pseudoHD_DD_unit:
            triangle.side_pseudoHD_DD = float(side)
        else:
            triangle.side_pseudoHD_DD = self.inch_to_meter(self.feet_inch_to_inch(side))

        # loading GT latlong for all three drones
        triangle.HD_GT_lat_long = PropertyReadWriteUtils.read_value_by_key(
            self.experiment_id + ".triangle.height." + str(height) + "m.HD_GT_lat_long")
        triangle.pseudoHD_GT_lat_long = PropertyReadWriteUtils.read_value_by_key(
            self.experiment_id + ".triangle.height." + str(height) + "m.pseudoHD_GT_lat_long")
        triangle.DD_GT_lat_long = PropertyReadWriteUtils.read_value_by_key(
            self.experiment_id + ".triangle.height." + str(height) + "m.DD_GT_lat_long")
        triangle.DD_prev_lat_long = PropertyReadWriteUtils.read_value_by_key(
            self.experiment_id + ".triangle.height." + str(height) + "m.DD_prev_lat_long")

        # loading GT bearing of HD_pseudoHD, HD_DD
        triangle.HD_pseudoHD_GT_bearing = PropertyReadWriteUtils.read_value_by_key(
            self.experiment_id + ".triangle.height." + str(height) + "m.HD_pseudoHD_GT_bearing")
        triangle.HD_DD_GT_bearing = PropertyReadWriteUtils.read_value_by_key(
            self.experiment_id + ".triangle.height." + str(height) + "m.HD_DD_GT_bearing")

        return triangle

    @staticmethod
    def feet_inch_to_inch(side):
        feet = 0
        inch = 0
        if side:
            feet_n_inch = side.split(',')
            if feet_n_inch is not None:
                it = iter(feet_n_inch)
                try:
                    feet = float(next(it))
                    inch = float(next(it))
                except StopIteration as e:
                    print(e)

        return (feet * 12) + inch

    @staticmethod
    def inch_to_meter(inch):
        if inch and inch > 0:
            return round(inch * 0.0254, 1)
        else:
            return '0'

    @staticmethod
    def solve_sss(a, b, c):
        angle_a = None
        angle_b = None
        angle_c = None
        note = None

        if not ProcessWiFiFtmData.is_valid_triangle(a, b, c):
            note = "Invalid triangle"
            print(note, a, b, c)
            return angle_a, angle_b, angle_c, note

        try:
            angle_c = round(math.degrees(math.acos((a ** 2 + b ** 2 - c ** 2) / (2 * a * b))), 2)
            angle_b = round(math.degrees(math.acos((c ** 2 + a ** 2 - b ** 2) / (2 * a * c))), 2)
            angle_a = round(math.degrees(math.acos((b ** 2 + c ** 2 - a ** 2) / (2 * b * c))), 2)
        except ValueError as e:
            print(e)
        return angle_a, angle_b, angle_c, note

    @staticmethod
    def is_valid_triangle(a, b, c):
        if a + b >= c and b + c >= a and c + a >= b:
            return True
        else:
            return False

    def process_wifi_data(self, triangle_height):
        merged_df = self.map_field_data(triangle_height)

        self.load_GTdata(merged_df, triangle_height)

        self.measure_sss_angles(merged_df)

        print(merged_df)
        merged_df.to_csv(
            self.local_path + '/' + ('merged_' + str(triangle_height) + '_m_wifi_' + self.experiment_id + '.csv'),
            index=False)

        return len(merged_df.index)

    def measure_sss_angles(self, merged_df):
        # find angles of all SSS
        angles = [self.solve_sss(side_a, side_b, side_c)
                  for side_a, side_b, side_c in
                  zip(merged_df['GT_side_pseudoHD_DD'], merged_df['side_HD_DD'], merged_df['side_HD_pseudoHD'])]
        merged_df[['angle_HD', 'angle_pseudoHD', 'angle_DD', 'Note']] = pd.DataFrame(angles)

    def load_GTdata(self, merged_df, triangle_height):
        # add the ground truth distance column for pseudo-HD and DD
        # respectively left(pseudo-HD)_GTdist, right(DD)_GTdist
        # add the column for distance between Pseduo-HD and DD from the ground marker
        gt_triangle = self.load_gt_triangle(triangle_height)
        merged_df['GT_side_pseudoHD_DD'] = gt_triangle.side_pseudoHD_DD
        merged_df['GT_side_HD_DD'] = gt_triangle.side_HD_DD
        merged_df['GT_side_HD_pseudoHD'] = gt_triangle.side_HD_pseudoHD
        # adding GT lat_long for all three drones in the csv
        if gt_triangle.HD_GT_lat_long is not None:
            HD_GT_lat, HD_GT_long = LatLonUtil.parse_lat_long((gt_triangle.HD_GT_lat_long).split(","))
            merged_df['HD_GT_lat'] = HD_GT_lat
            merged_df['HD_GT_long'] = HD_GT_long
        if gt_triangle.pseudoHD_GT_lat_long is not None:
            pseudoHD_GT_lat, pseudoHD_GT_long = LatLonUtil.parse_lat_long((gt_triangle.pseudoHD_GT_lat_long).split(","))
            merged_df['pseudoHD_GT_lat'] = pseudoHD_GT_lat
            merged_df['pseudoHD_GT_long'] = pseudoHD_GT_long
        if gt_triangle.DD_GT_lat_long is not None:
            DD_GT_lat, DD_GT_long = LatLonUtil.parse_lat_long((gt_triangle.DD_GT_lat_long).split(","))
            merged_df['DD_GT_lat'] = DD_GT_lat
            merged_df['DD_GT_long'] = DD_GT_long
        if gt_triangle.DD_prev_lat_long is not None:
            DD_prev_lat, DD_prev_long = LatLonUtil.parse_lat_long((gt_triangle.DD_prev_lat_long).split(","))
            merged_df['DD_prev_lat'] = DD_prev_lat
            merged_df['DD_prev_long'] = DD_prev_long

        # adding GT bearings
        if gt_triangle.HD_pseudoHD_GT_bearing is not None:
            merged_df['HD_pseudoHD_GT_bearing'] = gt_triangle.HD_pseudoHD_GT_bearing

        if gt_triangle.HD_DD_GT_bearing is not None:
            merged_df['HD_DD_GT_bearing'] = gt_triangle.HD_DD_GT_bearing

    def map_field_data(self, triangle_height):
        # read both the left(pseudo-HD)_dist and right(DD)_dist from corresponding left and right files
        left_prefixed_file_list = [f for f in os.listdir(self.raw_data_path) if
                                   f.startswith('left_' + str(triangle_height))]
        right_prefixed_file_list = [f for f in os.listdir(self.raw_data_path) if
                                    f.startswith('right_' + str(triangle_height))]
        left_drone_datafilename = left_prefixed_file_list[0]
        right_drone_datafilename = right_prefixed_file_list[0]
        pseudoHD_dataframe = pd.read_csv((self.raw_data_path + '/' + left_drone_datafilename),
                                         float_precision='round_trip')
        DD_dataframe = pd.read_csv((self.raw_data_path + '/' + right_drone_datafilename),
                                   float_precision='round_trip')
        timestamp_pseudoHD = pseudoHD_dataframe['#<Time(ms)>']
        timestamp_DD = DD_dataframe['#<Time(ms)>']
        side_HD_pseudoHD = round((pseudoHD_dataframe['<Est. Range(m)>'].astype(float)), 1)
        side_HD_DD = round((DD_dataframe['<Est. Range(m)>'].astype(float)), 1)
        # merge them starting from the first row, truncate the unmatched rows from the end of the file
        merged_df = pd.DataFrame()
        print(merged_df)
        # merged_df = pd.DataFrame(dist_btw_pseudoHD_n_HD, columns=["dist_btw_pseudoHD_n_HD"])
        merged_df['timestamp_pseudoHD'] = timestamp_pseudoHD
        merged_df['timestamp_DD'] = timestamp_DD
        merged_df['side_HD_pseudoHD'] = side_HD_pseudoHD
        merged_df['side_HD_DD'] = side_HD_DD
        return merged_df



# def __main():

#     experiment_id = 'exp2'
#     process_wifi_data = ProcessWiFiFtmData(experiment_id)
#
#     local_path = "data"
#     path = 'C:\khadija.csedu\MCPS\summer 2021\experiment-data\wifi-ftm-data\experiment2-HD-pseduHD-DD'
#
#     triangle_height = 3
#     process_wifi_data.process_wifi_data(triangle_height)
#
#     triangle_height = 5
#     process_wifi_data.process_wifi_data(triangle_height)
#
#     triangle_height = 7
#     process_wifi_data.process_wifi_data(local_path, path, triangle_height)
#
#     triangle_height = 10
#     process_wifi_data.process_wifi_data(local_path, path, triangle_height)
#
#     triangle_height = 15
#     process_wifi_data.process_wifi_data(local_path, path, triangle_height)
#
# if __name__ == '__main__':
#     __main()
