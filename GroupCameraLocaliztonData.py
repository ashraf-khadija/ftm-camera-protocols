import pandas as pd

class GroupCameraLocaliztonData:


    # TODO: Merge all 3 camera localization result datafile
    # TODO: round off the column 'est_dist_y'. As this column represents the estimated
    # distance between HD and DD
    # TODO: Upon rounding off we get distance between 3 meter to 15 meter. The below ranging
    # is used to round off the distance data into 5 distance group.
    # 2 <= dist <= 3.9 -> 3-meter-distance-group
    # 4 <= dist <= 5.9 -> 5-meter-distance-group
    # 6 <= dist <= 8.9 -> 7-meter-distance-group
    # 9 <= dist <= 10.9 -> 10-meter-distance-group
    # 11 <= dist <= 15 -> 15-meter-distance-group
    # TODO: once rounding off is done then we group the CSV data by seperating the files into 5 different
    # CSV by each distance group starting from 3-meter group to al the way to 15-meter group.
    # These 5 generated files will be the input data file for the drone swarm localization error assignment.

    def merge_camera_result_files (self, local_path,file1, file2, file3):
        #read test1 datafile
        result1_df = pd.read_csv((local_path + '/' + file1), float_precision='round_trip')

        mapped_est_dist_y = [self.label_dist_value(float(estimated_dist[0]))
                             for (estimated_dist) in
                             zip(result1_df['est_dist_y'])]
        result1_df[['mapped_est_dist_y']] = pd.DataFrame(mapped_est_dist_y)

        result2_df = pd.read_csv((local_path + '/' + file2), float_precision='round_trip')

        mapped_est_dist_y = [self.label_dist_value(float(estimated_dist[0]))
                             for (estimated_dist) in
                             zip(result2_df['est_dist_y'])]
        result2_df[['mapped_est_dist_y']] = pd.DataFrame(mapped_est_dist_y)

        result3_df = pd.read_csv((local_path + '/' + file3), float_precision='round_trip')

        mapped_est_dist_y = [self.label_dist_value(float(estimated_dist[0]))
                             for (estimated_dist) in
                             zip(result3_df['est_dist_y'])]
        result3_df[['mapped_est_dist_y']] = pd.DataFrame(mapped_est_dist_y)

        # concatenating df1 and df2 along rows
        temp_df = pd.concat([result1_df, result2_df], axis=0)
        merged_df = pd.concat([temp_df, result3_df], axis=0)

        # TODO: once rounding off is done then we group the CSV data by seperating the files into 5 different
        # CSV by each distance group starting from 3-meter group to al the way to 15-meter group.
        # These 5 generated files will be the input data file for the drone swarm localization error assignment.

        # Example 3: Split Dataframe using groupby() &
        # grouping by particular dataframe column
        grouped = merged_df.groupby(merged_df.mapped_est_dist_y)
        dist_group_3m_df = grouped.get_group(3)
        dist_group_5m_df = grouped.get_group(5)
        dist_group_7m_df = grouped.get_group(7)
        dist_group_10m_df = grouped.get_group(10)
        dist_group_15m_df = grouped.get_group(15)

        merged_df.to_csv(local_path + '/' + ('..\merged_camera_resultset_complete.csv'), index=False)

        dist_group_3m_df.to_csv(local_path + '/' + ('..\camera_resultset_3m.csv'), index=False)
        dist_group_5m_df.to_csv(local_path + '/' + ('..\camera_resultset_5m.csv'), index=False)
        dist_group_7m_df.to_csv(local_path + '/' + ('..\camera_resultset_7m.csv'), index=False)
        dist_group_10m_df.to_csv(local_path + '/' + ('..\camera_resultset_10m.csv'), index=False)
        dist_group_15m_df.to_csv(local_path + '/' + ('..\camera_resultset_15m.csv'), index=False)

    def label_dist_value(self, dist_val):
        # 2 <= dist <= 3.9 -> 3-meter-distance-group
        # 4 <= dist <= 5.9 -> 5-meter-distance-group
        # 6 <= dist <= 8.9 -> 7-meter-distance-group
        # 9 <= dist <= 10.9 -> 10-meter-distance-group
        # 11 <= dist <= 15 -> 15-meter-distance-group
        if dist_val >= 0.0 and dist_val < 4.0:
            return 3
        elif dist_val >= 4.0 and dist_val < 6.0:
            return 5
        elif dist_val >= 6.0 and dist_val < 9.0:
            return 7
        elif dist_val >= 9.0 and dist_val < 11.0:
            return 10
        elif dist_val >= 11.0 and dist_val < 20.0:
            return 15

def __main():
    groupLocalize = GroupCameraLocaliztonData()

    local_path = "data\experiment1"
    datafile1 = "camera_localization_merged_test1-export.csv"
    datafile2 = "camera_localization_merged_test2-export.csv"
    datafile3 = "camera_localization_merged_test3-export.csv"

    groupLocalize.merge_camera_result_files(local_path, datafile1, datafile2, datafile3)


if __name__ == '__main__':
    __main()

