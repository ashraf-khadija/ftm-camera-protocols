import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


class PlotBehaviorSpaceData:

    def __init__(self, local_path):
        self.HD_SPEED = 10 # meter/sec
        self.WiFi_COMMUNICATION_DELAY = 0.02  # 20 millsecond
        self.LED_CAMERA_COMMUNICATION_DELAY = 60 #second
        self.LAT_LON_ESTIMATION_DELAY = 0.02  # 10 millsecond
        self.DD_DETECTION_IN_IMAGE = 0.32 # second
        self.SIMULATION_REPETITION_FACTOR = 1  # each simulation parameter combination runs this many times

        self.local_path = local_path

        self.blue_palette = ["#a2d2ff", "#cdb4db"]
        self.orange_palette = ["#ee9b00", "#9b2226"]
        self.green_palette = ["#ecf39e", "#31572c"]

    def process_behaviorspace_accuracy_raw_data(self):
        #convert the rows to columns
        df = pd.read_csv(self.local_path + '/' + 'swarm-localization - behaviourSpace experiment.csv',
            float_precision='round_trip', skiprows =6, nrows=5)

        print(df)

        transposed_df = df.T

        print(transposed_df[0][0])
        print(transposed_df[1][0])
        print(transposed_df[2][0])
        print(transposed_df[3][0])
        print(transposed_df[4][0])

        # del transposed_df['reporter']
        transposed_df[3][0] = (transposed_df[3][0]).replace('[', '')
        transposed_df[3][0] = (transposed_df[3][0]).replace(']', '')
        transposed_df[4][0] = (transposed_df[4][0]).replace('[', '')
        transposed_df[4][0] = (transposed_df[4][0]).replace(']', '')

        print(transposed_df[3][0])
        print(transposed_df[4][0])

        del transposed_df[3]
        print(transposed_df)

        transposed_df[4][0] = 'avg-localization-error'

        transposed_df.to_csv(self.local_path + '/' + ('behaviourSpace_transposed.csv'),
                             header=False)

    def plot_location_error_data(self):
        # df = pd.read_csv(self.local_path + '/' + ('behaviourSpace_transposed.csv'),
        #                  float_precision='round_trip')

        df = pd.read_csv(self.local_path + '/' + ('behaviourSpace_transposed_error_patch_pairing_count.csv'),
                         float_precision='round_trip')

        print(df.columns)

        # Boxplot of distance_group vs accuracy
        sns.set_context("notebook", font_scale=1.5)
        plt.figure(figsize=(10, 8))
        sns.set_palette(self.orange_palette)


        sns.catplot(
            x='walker-vision-dist',
            y='avg-localization-error',
            hue='localization-mode',
            kind="box",
            legend=False,
            data=df,
            height=6,
            aspect=1.3)

        plt.xlabel("Distance Group", size=24)
        plt.ylabel("Avg. Localization Error(m)", size=24)

        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.xticks([0, 1, 2, 3, 4], ['3', '5', '7', '10', '15'])

        plt.grid(color='gray', linestyle='--', linewidth=0.5)
        plt.tight_layout()
        plt.legend(prop={'size': 20})
        plt.savefig("data/plot/walker-vision_vs_accuracy_camera_wifi_data.png")
        plt.clf()


        # Boxplot of dd-count vs accuracy
        sns.set_context("notebook", font_scale=1.5)
        plt.figure(figsize=(10, 8))
        sns.set_palette(self.blue_palette)

        sns.catplot(
            x='dd-count',
            y='avg-localization-error',
            hue='localization-mode',
            kind="box",
            legend=False,
            data=df,
            height=6,
            aspect=1.3)

        plt.xlabel('DD Count', size=24)
        plt.ylabel('Avg. Localization Error(m)', size=24)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.grid(color='gray', linestyle='--', linewidth=0.5)
        plt.tight_layout()
        plt.legend(prop={'size': 20})

        plt.savefig("data/plot/dd-count_vs_accuracy_camera_wifi_data.png")
        plt.clf()

    # this method plots the actual netlogo model execution time, not the experiment timing analysis data
    def process_behaviorspace_timer_raw_data(self):
        # convert the rows to columns
        df = pd.read_csv(self.local_path + '/' + 'swarm-localization - behaviourSpace experiment-timer.csv',
                         float_precision='round_trip', skiprows=6, nrows=5)

        print(df)

        transposed_df = df.T

        print(transposed_df[0][0])
        print(transposed_df[1][0])
        print(transposed_df[2][0])
        print(transposed_df[3][0])
        print(transposed_df[4][0])

        # del transposed_df['reporter']
        transposed_df[3][0] = (transposed_df[3][0]).replace('[', '')
        transposed_df[3][0] = (transposed_df[3][0]).replace(']', '')
        transposed_df[4][0] = (transposed_df[4][0]).replace('[', '')
        transposed_df[4][0] = (transposed_df[4][0]).replace(']', '')

        print(transposed_df[3][0])
        print(transposed_df[4][0])

        del transposed_df[3]
        print(transposed_df)

        transposed_df[4][0] = 'execution_time'

        transposed_df.to_csv(self.local_path + '/' + ('behaviourSpace_transposed_timer.csv'),
                             header=False)

    # this method plots the experiment timing analysis data

    def plot_timer_data(self):
        # df = pd.read_csv(self.local_path + '/' + ('behaviourSpace_transposed_timer.csv'),
        #                  float_precision='round_trip')

        df = pd.read_csv(self.local_path + '/' + ('behaviourSpace_with_timing_data.csv'),
                         float_precision='round_trip')

        print(df.columns)

        # Boxplot of dd-count vs execution_time
        sns.set_context("notebook", font_scale=1.5)
        plt.figure(figsize=(10, 8))
        sns.set_palette(self.green_palette)

        sns.catplot(
            x='dd-count',
            y='execution_time',
            hue='localization-mode',
            kind="box",
            legend=False,
            data=df,
            height=6,
            aspect=1.3
        )

        plt.xlabel('DD Count',  size=24)
        plt.ylabel('Total Localization Time(sec)', size=24)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.grid(color='gray', linestyle='--', linewidth=0.5)
        plt.tight_layout()
        plt.legend(prop={'size': 20})

        plt.savefig("data/plot/dd-count_vs_time_analysis_camera_wifi_data.png")
        plt.clf()

    def process_behaviorspace_patch_count_data(self):
        # convert the rows to columns
        df = pd.read_csv(self.local_path + '/'
                         + 'swarm-localization - behaviourSpace experiment-patch-traveled.csv',
                         float_precision='round_trip', skiprows=6, nrows=5)

        print(df)

        transposed_df = df.T

        print(transposed_df[0][0])
        print(transposed_df[1][0])
        print(transposed_df[2][0])
        print(transposed_df[3][0])
        print(transposed_df[4][0])

        # del transposed_df['reporter']
        transposed_df[3][0] = (transposed_df[3][0]).replace('[', '')
        transposed_df[3][0] = (transposed_df[3][0]).replace(']', '')
        transposed_df[4][0] = (transposed_df[4][0]).replace('[', '')
        transposed_df[4][0] = (transposed_df[4][0]).replace(']', '')

        print(transposed_df[3][0])
        print(transposed_df[4][0])

        del transposed_df[3]
        print(transposed_df)

        transposed_df[4][0] = 'patch_count'

        transposed_df.to_csv(self.local_path + '/' + ('behaviourSpace_transposed_patch_count.csv'),
                             header=False)

    def process_behaviorspace_patch_pairing_count_data(self):
        # convert the rows to columns
        df = pd.read_csv(self.local_path + '/'
                     + 'swarm-localization - behaviourSpace experiment-patch-traveled-pairing.csv',
                     float_precision='round_trip', skiprows=6, nrows=5)

        transposed_df = df.T

        transposed_df[3][0] = (transposed_df[3][0]).replace('[', '')
        transposed_df[3][0] = (transposed_df[3][0]).replace(']', '')
        transposed_df[4][0] = (transposed_df[4][0]).replace('[', '')
        transposed_df[4][0] = (transposed_df[4][0]).replace(']', '')

        processed_df = pd.DataFrame(columns=['localization-mode', 'walker-vision-dist', 'dd-count',
                                           'patch-count', 'pairing-count'])

        print(processed_df)
        for i, g in transposed_df.groupby([3]):
            print(g)
            # print(g[4])
            if len(g.index) > 1:
                if g[3][0] == 'get-total-patchs-traveled':
                    processed_df['localization-mode'] = g[0].values
                    processed_df['walker-vision-dist'] = g[1].values
                    processed_df['dd-count'] = g[2].values
                    processed_df['patch-count'] = g[4].values
                elif g[3][0] == 'get-total-pairing-count':
                    processed_df['pairing-count'] = g[4].values

        print("processed_df: ")
        print(processed_df)

        processed_df.to_csv(self.local_path + '/'
                         + ('behaviourSpace_transposed_patch_pairing_count.csv'))


    def process_behaviorspace_error_patch_pairing_count_data(self):
        # convert the rows to columns
        df = pd.read_csv(self.local_path + '/'
                     + 'swarm-localization - behaviourSpace experiment-error-patch-pairing.csv',
                     float_precision='round_trip', skiprows=6, nrows=5)

        transposed_df = df.T

        processed_df = pd.DataFrame(columns=['localization-mode', 'walker-vision-dist', 'dd-count',
                                             'avg-localization-error', 'patch-count', 'pairing-count'])

        print(processed_df)
        for i, g in transposed_df.groupby([3]):
            print(g)
            # print(g[4])
            if len(g.index) > 1:
                if g[3][0] == 'get-avg-localize-error':
                    processed_df['localization-mode'] = g[0].values
                    processed_df['walker-vision-dist'] = g[1].values
                    processed_df['dd-count'] = g[2].values
                    processed_df['avg-localization-error'] = g[4].values
                elif g[3][0] == 'get-total-pairing-count':
                    processed_df['pairing-count'] = g[4].values
                elif g[3][0] == 'get-total-patchs-traveled':
                    processed_df['patch-count'] = g[4].values


        print("processed_df: ")
        print(processed_df)

        processed_df.to_csv(self.local_path + '/'
                         + ('behaviourSpace_transposed_error_patch_pairing_count.csv'))

    def process_behaviorspace_all_repetition(self):
        # convert the rows to columns
        df = pd.read_csv(self.local_path + '/'
                     + 'swarm-localization - behaviourSpace experiment-error-patch-pairing-rep-4.csv',
                     float_precision='round_trip', skiprows=6, nrows=5)

        transposed_df = df.T

        semi_processed_df = pd.DataFrame(columns=['localization-mode', 'walker-vision-dist', 'dd-count',
                                             'avg-localization-error', 'patch-count', 'pairing-count'])

        for i, g in transposed_df.groupby([3]):
            # print(g)
            if len(g.index) > 1:
                if g[3][0] == 'get-avg-localize-error':
                    semi_processed_df['localization-mode'] = g[0].values
                    semi_processed_df['walker-vision-dist'] = g[1].values
                    semi_processed_df['dd-count'] = g[2].values
                    semi_processed_df['avg-localization-error'] = g[4].values
                elif g[3][0] == 'get-total-pairing-count':
                    semi_processed_df['pairing-count'] = g[4].values
                elif g[3][0] == 'get-total-patchs-traveled':
                    semi_processed_df['patch-count'] = g[4].values

        semi_processed_df['avg-localization-error'] = semi_processed_df['avg-localization-error'].astype(float)
        semi_processed_df['patch-count'] = semi_processed_df['patch-count'].astype(int)
        semi_processed_df['pairing-count'] = semi_processed_df['pairing-count'].astype(int)

        final_group_df = pd.DataFrame(columns=['localization-mode', 'walker-vision-dist', 'dd-count',
                                             'avg-localization-error', 'patch-count', 'pairing-count'])

        pairing_count_group_summed = semi_processed_df.groupby(
            ['localization-mode', 'walker-vision-dist', 'dd-count'])['pairing-count'].sum()

        patch_count_group_summed = semi_processed_df.groupby(
            ['localization-mode', 'walker-vision-dist', 'dd-count'])['patch-count'].sum()

        avg_error_group_summed = semi_processed_df.groupby(
            ['localization-mode', 'walker-vision-dist', 'dd-count'])['avg-localization-error'].sum()

        grouped_error_df = avg_error_group_summed.to_frame()
        grouped_patch_count_df = patch_count_group_summed.to_frame()
        grouped_pairing_count_df = pairing_count_group_summed.to_frame()
        # final_group_df = pairing_count_group_summed
        final_group_df['avg-localization-error'] = grouped_error_df['avg-localization-error']
        final_group_df['patch-count'] = grouped_patch_count_df['patch-count']
        final_group_df['pairing-count'] = grouped_pairing_count_df['pairing-count']

        del final_group_df['localization-mode']
        del final_group_df['walker-vision-dist']
        del final_group_df['dd-count']

        final_group_df['avg-localization-error'] = final_group_df['avg-localization-error'].\
                                div(self.SIMULATION_REPETITION_FACTOR)

        final_group_df['pairing-count'] = final_group_df['pairing-count'].\
                                div(self.SIMULATION_REPETITION_FACTOR).astype(int)

        final_group_df['patch-count'] = final_group_df['patch-count']. \
            div(self.SIMULATION_REPETITION_FACTOR).astype(int)


        semi_processed_df.to_csv(self.local_path + '/' + ('group_data_semi_processed.csv'))
        final_group_df.to_csv(self.local_path + '/' + ('group_data_finally_processed.csv'))

    def time_calculation(self):
        df = pd.read_csv(self.local_path + '/'
                         + 'behaviourSpace_transposed_error_patch_pairing_count.csv',
                         float_precision='round_trip')

        # scanning_time (sec) = HD_travel_time + total_drone_detection_time
        HD_travel_times = [self.find_HD_travel_time(patch_count)
                                 for (patch_count) in
                                 zip(df['patch-count'])]

        df[['HD_travel_time']] = pd.DataFrame(HD_travel_times)

        drone_detection_times = [self.find_drone_detection_time(patch_count)
                                   for (patch_count) in
                                   zip(df['patch-count'])]

        df[['drone_detection_time']] = pd.DataFrame(drone_detection_times)

        scanning_times = [travel_time + detection_time
                            for (travel_time, detection_time) in
                            zip(HD_travel_times, drone_detection_times)]


        df[['scanning_time']] = pd.DataFrame(scanning_times)

        # pairing_time = (LED_camera_comm_time + WiFi_comm_time) * pairing_count
        pairing_times = [self.find_pairing_time(pairing_count)
                           for (pairing_count) in
                           zip(df['pairing-count'])]

        df[['pairing_time']] = pd.DataFrame(pairing_times)

        # estimation_communication_time = lat_lon_estimation_time + lat_lon_comm_time
        estimation_communication_times = [self.find_estimation_communication_time(pairing_count)
                                                 for (pairing_count) in
                                                 zip(df['pairing-count'])]

        df[['estimation_communication_time']] = pd.DataFrame(estimation_communication_times)

        execution_times = [scanning + pairing + communication
                                for (scanning, pairing, communication) in
                                zip(scanning_times, pairing_times, estimation_communication_times)]

        df[['execution_time']] = pd.DataFrame(execution_times)

        df.to_csv(self.local_path + '/' + ('behaviourSpace_with_timing_data.csv'))

    def find_HD_travel_time(self, patch_count):
        HD_travel_distance_in_meter = patch_count[0] * 3
        HD_travel_time = HD_travel_distance_in_meter / self.HD_SPEED
        return HD_travel_time

    def find_drone_detection_time(self, patch_count):
        number_of_scanning = patch_count[0] / 5  # means, in every 5 blocks a scanning happens
        total_drone_detection_time = number_of_scanning * self.DD_DETECTION_IN_IMAGE
        return total_drone_detection_time

    def find_pairing_time(self, pairing_count):
        pairing_time = (self.LED_CAMERA_COMMUNICATION_DELAY + self.WiFi_COMMUNICATION_DELAY) * pairing_count[0]
        return pairing_time

    def find_estimation_communication_time(self, pairing_count):
        estimation_communication_time = (self.LAT_LON_ESTIMATION_DELAY + self.WiFi_COMMUNICATION_DELAY) \
                                        * pairing_count[0]
        return estimation_communication_time


def __main():
    local_path = "data/behaviorspace"

    plot = PlotBehaviorSpaceData(local_path)
    # Single iteration of each parameter combination
    plot.process_behaviorspace_error_patch_pairing_count_data()
    plot.plot_location_error_data()
    plot.time_calculation()
    plot.plot_timer_data()

    # Multiple iteration of each parameter combination
    plot.SIMULATION_REPETITION_FACTOR = 4
    plot.process_behaviorspace_all_repetition()

if __name__ == '__main__':
    __main()