import numpy as np
import math

class CameraObjectAngleUtil:
    def __init__(self):
        self.x1 = None                          # pixel point 1, (x1, y1)
        self.y1 = None                          # pixel point 2, (x2, y2
        self.x2 = None
        self.y2 = None
        self.K = None                           # camera intrinsic parameters

    def find_angle(self):
        Ki = np.linalg.inv(self.K)
        vector_1 = Ki.dot([self.x1, self.y1, 1.0])
        vector_2 = Ki.dot([self.x2, self.y2, 1.0])
        print("r1: ", vector_1)
        print("r2: ", vector_2)

        unit_vector_1 = vector_1 / np.linalg.norm(vector_1)
        unit_vector_2 = vector_2 / np.linalg.norm(vector_2)

        dot_product = np.dot(unit_vector_1, unit_vector_2)

        angle = np.arccos(dot_product)
        print("angle_radian : ", angle)
        print("angle_degree : ", math.degrees(angle))

def __main():
    angleUtil = CameraObjectAngleUtil()
    # Ground truth angle 21.6
    # angleUtil.x1 = 1730
    # angleUtil.y1 = 1711
    # angleUtil.x2 = 2747
    # angleUtil.y2 = 1691
    #
    # # Gopro camera calibration 1 with low light
    # angleUtil.K = np.array([[3764.4, 0, 1948.3],
    #                         [0, 3735.9, 1420.3],
    #                         [0, 0, 1]])
    # estimated angle 15.3

    angleUtil.x1 = 1744
    angleUtil.y1 = 1331
    angleUtil.x2 = 2754
    angleUtil.y2 = 1359
    # Gopro camera calibration 2 with night mode
    angleUtil.K = np.array([[3600.1, 0, 1980.8],
                            [0, 3575.5, 1534.1],
                            [0, 0,      1]])
    # estimated angle 15.8
    angleUtil.find_angle()

if __name__ == '__main__':
    __main()