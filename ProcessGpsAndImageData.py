import pandas as pd
import os
from PropertyReadWriteUtils import *
from LatLonUtil import *

class ProcessGpsAndImageData:

    def __init__(self, experiment_id, raw_data_path, local_path):
        self.experiment_id = experiment_id
        self.raw_data_path = raw_data_path
        self.local_path = local_path

    def remove_duplicate_row_by_col_val(self, cols, dup_chek_columns, dataframe):
        # http://blog.adeel.io/2016/10/30/removing-neighboring-consecutive-only-duplicates-in-a-pandas-dataframe/
        # https://stackoverflow.com/questions/19463985/pandas-drop-consecutive-duplicates
        return dataframe[cols].loc[
            (dataframe[dup_chek_columns].shift() != dataframe[dup_chek_columns]).any(
                axis=1)]

    def clean_gps_data(self, path, filename, local_path):
        dataframe = pd.read_csv((path + '/' + filename), float_precision='round_trip')

        # removing dulplicate rows based on duplicate lat-lng values,
        cols = ["Time", "GPS_Latitude", "GPS_Longitude"]
        dup_chek_columns = ["GPS_Latitude", "GPS_Longitude"]
        de_dup = self.remove_duplicate_row_by_col_val(cols, dup_chek_columns, dataframe)

        cleaned_df = pd.DataFrame(de_dup, columns=["Time", "GPS_Latitude", "GPS_Longitude"])
        # multiply the GPS file's time column by 100 to convert the time to nanoseconds.
        converted_time = cleaned_df['Time'] * 100
        converted_time = (converted_time / 1e9).astype(int)

        cleaned_df.insert(1, "time_in_sec", converted_time, True)
        # write the non-duplicate rows into another csv file
        cleaned_df.to_csv(local_path + '/' + ('cleaned_' + filename), index=False)
        return cleaned_df

    def clean_image_data(self, path, directory, local_path):
        # read the list of images

        image_list = sorted((f for f in os.listdir(path + '/' + directory) if not f.startswith('desktop.ini')),
                            key=str.lower)
        df = pd.DataFrame(image_list, columns=['image'])
        df['time_stamp'] = df['image'].str.slice(0, 19)
        df['time_in_sec'] = (df['time_stamp'].astype(float) / 1e9).astype(int)
        df.to_csv(local_path + '/' + ('cleaned_' + directory + '_img.csv'), index=False)
        return df

    def merge_two_dataframe(self, df1, df2, common_col_name, filename, local_path):
        merge_df = pd.merge(df1, df2, on=common_col_name)
        # write the non-duplicate rows into another csv file
        merge_df.to_csv(local_path + '/' + ('merged_' + filename), index=False)

    def merge_labeled_image_n_gpsdata(self, local_path, labeled_filename,
                                      image_gps_datafilename, test_id):
        # TODO: Process labeled data
        df = pd.read_csv((local_path + '/' + labeled_filename), float_precision='round_trip')

        # Remove rows with column value 'Drone'. since this row is redundant
        df_without_redundant_label = df[df['label'] != 'Drone']

        # merge the img labeled data with the GPS data
        df_image_data_with_gps = pd.read_csv((local_path + '/' + image_gps_datafilename),
                                             float_precision='round_trip')

        # remove any invalid GPS data
        df_image_data_with_gps = df_image_data_with_gps[df_image_data_with_gps['GPS_Latitude'] != 0]

        common_col_name = "image"
        merge_df = pd.merge(df_without_redundant_label, df_image_data_with_gps, on=common_col_name)

        # add the HD's gps data.
        HD_GT_lat_long = PropertyReadWriteUtils.read_value_by_key(
            self.experiment_id + "." + str(test_id) +".HD.lat.long")

        if HD_GT_lat_long is not None:
            HD_GT_lat, HD_GT_long = LatLonUtil.parse_lat_long(HD_GT_lat_long.split(","))
            merge_df['HD_GT_lat'] = HD_GT_lat
            merge_df['HD_GT_long'] = HD_GT_long

        merge_df.to_csv(local_path + '/' + ('merged_' + labeled_filename), index=False)

def __main():
    experiment_id = "exp1"
    local_path = "data\experiment1"
    raw_data_path = 'C:\khadija.csedu\MCPS\summer 2021\experiment-data\Organized Data\Experiment 1'
    test_id = "test1"
    process_data = ProcessGpsAndImageData(experiment_id, raw_data_path, local_path)

    ########### Process dataset Experiment 1 - Test1
    gps_df1 = process_data.clean_gps_data(raw_data_path, 'Test1_GPS.csv', local_path)

    directory = 'Test1'
    img_df1 = process_data.clean_image_data(raw_data_path, directory, local_path)

    process_data.merge_two_dataframe(img_df1, gps_df1, "time_in_sec", 'image_gps1.csv', local_path)

    labeled_filename = "test1-export.csv"
    image_gps_datafilename = "merged_image_gps1.csv"
    test_id = 'test1'
    process_data.merge_labeled_image_n_gpsdata(local_path, labeled_filename, image_gps_datafilename, test_id)

    ########### Process dataset Experiment 1 - Test2

    gps_df2 = process_data.clean_gps_data(raw_data_path, 'Test2_GPS.csv', local_path)

    directory = 'Test2'
    img_df2 = process_data.clean_image_data(raw_data_path, directory, local_path)

    # process_data.merge_img_and_gps_data(gps_df2, img_df2,  'test2.csv', local_path)
    process_data.merge_two_dataframe(img_df2, gps_df2, "time_in_sec", 'image_gps2.csv', local_path)

    labeled_filename = "test2-export.csv"
    image_gps_datafilename = "merged_image_gps2.csv"
    test_id = 'test2'
    process_data.merge_labeled_image_n_gpsdata(local_path, labeled_filename, image_gps_datafilename, test_id)

    ########### Process dataset Experiment 1 - Test3

    gps_df3 = process_data.clean_gps_data(raw_data_path, 'Test3_GPS.csv', local_path)

    directory = 'Test3'
    img_df3 = process_data.clean_image_data(raw_data_path, directory, local_path)

    process_data.merge_two_dataframe(img_df3, gps_df3, "time_in_sec", 'image_gps3.csv', local_path)

    test_id = 'test3'
    labeled_filename = "test3-export.csv"
    image_gps_datafilename = "merged_image_gps3.csv"
    process_data.merge_labeled_image_n_gpsdata(local_path, labeled_filename, image_gps_datafilename, test_id)

if __name__ == '__main__':
    __main()
