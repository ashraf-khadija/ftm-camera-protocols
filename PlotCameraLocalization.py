import pandas as pd
import matplotlib.pyplot as plt

class PlotCameraLocalization:
    def __init__(self, experiment_id, local_path):
        self.experiment_id = experiment_id
        self.local_path = local_path

    def plot_all_metrics(self):
        # merge_all_location_diff
        test_id = 'test1'
        df1 = pd.read_csv(self.local_path + '/' + ('camera_localization_merged_' + test_id + '-export.csv'),
                         float_precision='round_trip')

        test_id = 'test2'
        df2 = pd.read_csv(self.local_path + '/' + ('camera_localization_merged_' + test_id + '-export.csv'),
                          float_precision='round_trip')

        test_id = 'test3'
        df3 = pd.read_csv(self.local_path + '/' + ('camera_localization_merged_' + test_id + '-export.csv'),
                          float_precision='round_trip')

        df = pd.concat([df1, df2, df3])
        # plot all distance differences between GT GPS-camera estimated GPS and
        # distance difference between ground marker and camera-estimated-distance
        # a in a single graph

        df.insert(0, 'id', range(1, 1 + len(df)))

        time = df['id']
        est_gps_x_diff_gt_gps = df['est_gps_x_diff_gt_gps']
        est_gps_y_diff_gt_gps = df['est_gps_y_diff_gt_gps']
        cam_est_dist_x_diff_marker_dist = df['cam_est_dist_x_diff_marker_dist']
        cam_est_dist_y_diff_marker_dist = df['cam_est_dist_y_diff_marker_dist']

        plt.xlabel("Time", size=24)
        plt.ylabel("Error(m)", size=24)
        plt.ylim([-1, 15])
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.grid(color='gray', linestyle='--', linewidth=0.5)
        # plt.rcParams['font.size'] = '16'
        plt.tight_layout()
        # plt.rcParams.update({'figure.autolayout': True})

        # plt.plot(time, est_gps_x_diff_gt_gps, label="GPS error using cam-x")
        plt.plot(time, est_gps_y_diff_gt_gps, label="GPS Error", c='tab:blue')
        # plt.plot(time, cam_est_dist_x_diff_marker_dist, label="Cam-x distance error")
        plt.plot(time, cam_est_dist_y_diff_marker_dist, label="Distance Error", c='tab:red')
        # plot.legend(loc=2, prop={'size': 6})

        # https: // stackoverflow.com / questions / 4700614 / how - to - put - the - legend - outside - the - plot - in -matplotlib
        # plt.legend(bbox_to_anchor=(0, 1.02, 1, 0.2), , loc="lower left",
        #            mode="expand", borderaxespad=0, ncol=2, prop={'size': 14})
        plt.legend(prop={'size': 20})
        plt.savefig('data/plot/' + self.experiment_id + '/GPS_n_distance_error_attdata.png')
        plt.clf()

        # plotting distance differences between GT GPS-camera estimated GPS

        est_gps_x_diff_gt_gps = df['est_gps_x_diff_gt_gps']
        est_gps_y_diff_gt_gps = df['est_gps_y_diff_gt_gps']

        plt.xlabel("Time", size=24)
        plt.ylabel("Error(m)", size=24)
        plt.ylim([-1, 15])
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.grid(color='gray', linestyle='--', linewidth=0.5)
        # plt.rcParams['font.size'] = '16'
        plt.tight_layout()
        # plt.rcParams.update({'figure.autolayout': True})

        # plt.plot(time, est_gps_x_diff_gt_gps, label="GPS error using cam-x")
        # plt.plot(time, est_gps_y_diff_gt_gps, label="GPS error using cam-y")
        plt.plot(time, est_gps_y_diff_gt_gps, label="GPS Error", c='tab:blue')

        plt.legend(prop={'size': 20})

        std_x = est_gps_x_diff_gt_gps.std()
        std_y = est_gps_y_diff_gt_gps.std()

        print("std GPS error: ", std_x, std_y)
        print("mean GPS error: ", est_gps_x_diff_gt_gps.mean(), est_gps_y_diff_gt_gps.mean())

        # https: // stackoverflow.com / questions / 4700614 / how - to - put - the - legend - outside - the - plot - in -matplotlib
        # plt.legend(bbox_to_anchor=(0, 1.02, 1, 0.2), loc="lower left",
        #            mode="expand", borderaxespad=0, ncol=2)
        plt.savefig('data/plot/' + self.experiment_id + '/GPS_error_attdata.png')
        plt.clf()

        # plotting only distance difference between ground marker and camera-estimated-distance

        # est_gps_x_diff_gt_gps = df['est_gps_x_diff_gt_gps']
        # est_gps_y_diff_gt_gps = df['est_gps_y_diff_gt_gps']
        cam_est_dist_x_diff_marker_dist = df['cam_est_dist_x_diff_marker_dist']
        cam_est_dist_y_diff_marker_dist = df['cam_est_dist_y_diff_marker_dist']

        std_x = cam_est_dist_x_diff_marker_dist.std()
        std_y = cam_est_dist_y_diff_marker_dist.std()

        print("std dist error: ", std_x, std_y)
        print("mean dist error: ", cam_est_dist_x_diff_marker_dist.mean(), cam_est_dist_y_diff_marker_dist.mean())

        plt.xlabel("Time", size=24)
        plt.ylabel("Error(m)", size=24)
        plt.ylim([-1, 15])
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.grid(color='gray', linestyle='--', linewidth=0.5)
        # plt.rcParams['font.size'] = '16'
        plt.tight_layout()
        # plt.rcParams.update({'figure.autolayout': True})
        plt.legend(prop={'size': 20},  loc="upper left")

        # plt.plot(time, cam_est_dist_x_diff_marker_dist, color='g', label="Cam-x distance error")
        # plt.plot(time, cam_est_dist_y_diff_marker_dist,  color='firebrick', label="Cam-y distance error")
        plt.plot(time, cam_est_dist_y_diff_marker_dist, label="Distance Error", c='tab:red')

        # https: // stackoverflow.com / questions / 4700614 / how - to - put - the - legend - outside - the - plot - in -matplotlib
        # plt.legend(bbox_to_anchor=(0, 1.02, 1, 0.2), loc="lower left",
        #            mode="expand", borderaxespad=0, ncol=2)
        plt.savefig('data/plot/' + self.experiment_id + '/distance_error_attdata.png')
        plt.clf()

        #plotting the bearing
        est_bearing_btwn_cam_obj = df['est_bearing_btwn_cam_obj']

        plt.xlabel("Time", size=24)
        plt.ylabel("Bearing(deg)", size=24)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        # plt.rcParams['font.size'] = '16'
        plt.tight_layout()
        # plt.rcParams.update({'figure.autolayout': True})
        
        plt.scatter(time, est_bearing_btwn_cam_obj, label="Estimate bearing")
        plt.savefig('data/plot/' + self.experiment_id + '/baring_all_data.png')
        plt.clf()

def __main():
    experiment_id = 'exp1'
    local_path = "data/experiment1"

    plot = PlotCameraLocalization(experiment_id, local_path)

    plot.plot_all_metrics()

if __name__ == '__main__':
    __main()