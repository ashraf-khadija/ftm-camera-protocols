# TODO: Run this file to execute Experiment 2. Includes, data processing, location estimation, evaluation, graph plotting

from ProcessWiFiFtmData import *
from WiFiFtmLocalizationExp2 import *
from EvaluationWiFiFtmLocalization import *
from LatLonUtil import *
import time


class Experiment2(ProcessWiFiFtmData, WiFiFtmLocalizationExp2, EvaluationWiFiFtmLocalization):
    def __init__(self, experiment_id, raw_data_path, local_path):
        ProcessWiFiFtmData.__init__(self, experiment_id, raw_data_path, local_path)
        WiFiFtmLocalizationExp2.__init__(self, experiment_id, local_path)
        EvaluationWiFiFtmLocalization.__init__(self, experiment_id, local_path)

def __main():
    experiment_id = 'exp2'
    local_path = "data/experiment2"
    raw_data_path = 'E:\khadija.csedu-backup\MCPS\summer 2021\experiment-data\wifi-ftm-data\experiment2-HD-pseduHD-DD'

    exp2 = Experiment2(experiment_id, raw_data_path, local_path)

    triangle_height = 3
    tic = time.perf_counter()
    num_of_records = exp2.process_wifi_data(triangle_height)
    exp2.estimate_DD_location(triangle_height)
    toc = time.perf_counter()
    execution_time_per_record = (toc - tic) / num_of_records

    print("total_execution_time: ", (toc - tic))
    print("num_of_records: ", num_of_records)
    print("execution_time_per_record: ", execution_time_per_record)

    exp2.accuracy_measurement(triangle_height)

    triangle_height = 5
    exp2.process_wifi_data(triangle_height)
    exp2.estimate_DD_location(triangle_height)
    exp2.accuracy_measurement(triangle_height)

    triangle_height = 7
    exp2.process_wifi_data(triangle_height)
    exp2.estimate_DD_location(triangle_height)
    exp2.accuracy_measurement(triangle_height)

    triangle_height = 10
    exp2.process_wifi_data(triangle_height)
    exp2.estimate_DD_location(triangle_height)
    exp2.accuracy_measurement(triangle_height)

    triangle_height = 15
    exp2.process_wifi_data(triangle_height)
    exp2.estimate_DD_location(triangle_height)
    exp2.accuracy_measurement(triangle_height)

if __name__ == '__main__':
    __main()
