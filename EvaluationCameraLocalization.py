from LatLonUtil import *
from Utility import *

class EvaluationWiFiFtmLocalization:
    def __init__(self, experiment_id, local_path):
        self.experiment_id = experiment_id
        self.local_path = local_path

    def accuracy_measurement(self, test_id):
        df = pd.read_csv(self.local_path + '/' + ('camera_localization_merged_'+test_id+'-export.csv'),
                         float_precision='round_trip')

        # Estimated location accuracy-x
        est_gps_x_diff_gt_gps = [LatLonUtil.dist(lat1, long1, lat2, long2)
                                          for (lat1, long1, lat2, long2) in
                                          zip(df['GPS_Latitude'], df['GPS_Longitude'],
                                              df['lat_x'], df['long_x'])]

        df[['est_gps_x_diff_gt_gps']] = pd.DataFrame(est_gps_x_diff_gt_gps)

        # Estimated location accuracy-y
        est_gps_y_diff_gt_gps = [LatLonUtil.dist(lat1, long1, lat2, long2)
                                            for (lat1, long1, lat2, long2) in
                                            zip(df['GPS_Latitude'], df['GPS_Longitude'],
                                                df['lat_y'], df['long_y'])]

        df[['est_gps_y_diff_gt_gps']] = pd.DataFrame(est_gps_y_diff_gt_gps)

        # Estimated cameraX-distance accuracy
        cam_est_dist_x_diff_marker_dist = [Utility.find_diff(estimated_dist, GT_dist)
                                  for (estimated_dist, GT_dist) in
                                  zip(df['est_dist_x'], df['label'])]

        df[['cam_est_dist_x_diff_marker_dist']] = pd.DataFrame(cam_est_dist_x_diff_marker_dist)

        # Estimated cameraY-distance accuracy
        cam_est_dist_y_diff_marker_dist = [Utility.find_diff(estimated_dist, GT_dist)
                            for (estimated_dist, GT_dist) in
                            zip(df['est_dist_y'], df['label'])]

        df[['cam_est_dist_y_diff_marker_dist']] = pd.DataFrame(cam_est_dist_y_diff_marker_dist)

        # # Estimated bearing accuracy
        # HD_DD_bearing_diffs = [Utility.find_bearing_diff(est_bearing, GT_bearing)
        #                     for (est_bearing, GT_bearing) in
        #                     zip(df['HD_DD_est_bearing'], df['HD_DD_GT_bearing'])]
        #
        # df[['HD_DD_bearing_diffs']] = pd.DataFrame(HD_DD_bearing_diffs)

        df.to_csv(self.local_path + '/' + ('camera_localization_merged_'+test_id+'-export.csv'),
                         index=False)

def __main():
    experiment_id = 'exp1'
    local_path = "data/experiment1"

    evaluation = EvaluationWiFiFtmLocalization(experiment_id, local_path)

    test_id = 'test1'
    evaluation.accuracy_measurement(test_id)

    test_id = 'test2'
    evaluation.accuracy_measurement(test_id)

    test_id = 'test3'
    evaluation.accuracy_measurement(test_id)

if __name__ == '__main__':
    __main()