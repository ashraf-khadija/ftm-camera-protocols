import numpy as np
import math
from PropertyReadWriteUtils import *
from ProcessGpsAndImageData import *
from enum import Enum
import time

class CameraBasedLocalization:
    class Direction(Enum):
        LEFT = 1
        RIGHT = 2

    def __init__(self):
        self.fx = float(PropertyReadWriteUtils.read_value_by_key("FOCAL_LENGTH_X"))
        self.fy = float(PropertyReadWriteUtils.read_value_by_key("FOCAL_LENGTH_Y"))
        self.cx = float(PropertyReadWriteUtils.read_value_by_key("cx"))
        self.cy = float(PropertyReadWriteUtils.read_value_by_key("cy"))

        self.K = np.array([[self.fx, 0, self.cx],
                           [0, self.fy, self.cy],
                           [0, 0,  1]])

    def find_angle(self, x1, y1, x2, y2):
        Ki = np.linalg.inv(self.K)
        vector_1 = Ki.dot([x1, y1, 1.0])
        vector_2 = Ki.dot([x2, y2, 1.0])
        # print("r1: ", vector_1)
        # print("r2: ", vector_2)

        unit_vector_1 = vector_1 / np.linalg.norm(vector_1)
        unit_vector_2 = vector_2 / np.linalg.norm(vector_2)

        dot_product = np.dot(unit_vector_1, unit_vector_2)

        angle_radian = np.arccos(dot_product)
        angle_degree = math.degrees(angle_radian)
        # print("angle_radian : ", angle)
        # print("angle_degree : ", math.degrees(angle))
        return angle_degree

    @staticmethod
    def calculate_d(x, y, x1, y1, x2, y2):
        return ((x - x1) * (y2 - y1)) - ((y - y1) * (x2 - x1))

    @staticmethod
    def if_point_on_left_or_right(x, y, x1, y1, x2, y2):
        reference_left_point = (x1-1, y1)
        reference_d = CameraBasedLocalization.calculate_d(reference_left_point[0], reference_left_point[1],
                                                          x1, y1, x2, y2)
        d = CameraBasedLocalization.calculate_d(x, y, x1, y1, x2, y2)

        if (d * reference_d) > 0:
            return CameraBasedLocalization.Direction.LEFT
        else:
            return CameraBasedLocalization.Direction.RIGHT

    def find_bearing(self, xmin, ymin, xmax, ymax):
        cam_localize = CameraBasedLocalization()

        img_resolution_x = float(PropertyReadWriteUtils.read_value_by_key("IMAGE_RESOLUTION_X"))
        img_resolution_y = float(PropertyReadWriteUtils.read_value_by_key("IMAGE_RESOLUTION_Y"))
        image_mid_line_bottom_pixel = (img_resolution_x/2, img_resolution_y)
        image_mid_line_top_pixel = (img_resolution_x/2, 0)
        bounding_box_midpoint = (xmin + (xmax - xmin)/2, ymin + (ymax - ymin)/2)

        x = bounding_box_midpoint[0]
        y = bounding_box_midpoint[1]

        x1 = image_mid_line_top_pixel[0]
        y1 = image_mid_line_top_pixel[1]

        x2 = image_mid_line_bottom_pixel[0]
        y2 = image_mid_line_bottom_pixel[1]

        position = cam_localize.if_point_on_left_or_right(x, y, x1, y1, x2, y2)

        bearing = cam_localize.find_angle(image_mid_line_bottom_pixel[0], image_mid_line_bottom_pixel[1],
                                          bounding_box_midpoint[0], bounding_box_midpoint[0])
        if position == cam_localize.Direction.LEFT:
            return 360 - bearing
        else:
            return bearing

    def find_distance(self, xmin, ymin, xmax, ymax):
        object_image_width = xmax - xmin
        object_image_height = ymax - ymin

        object_real_width = PropertyReadWriteUtils.read_value_by_key("DRONE_WIDTH_IN_MM")
        object_real_height = PropertyReadWriteUtils.read_value_by_key("DRONE_HEIGHT_IN_MM")

        # pin-hole camera geometry
        distance_x = round((((self.fx * float(object_real_width)) / object_image_width) / 1000), 1)
        distance_y = round((((self.fy * float(object_real_height)) / object_image_height) / 1000), 1)

        return distance_x, distance_y

    # TODO: derive estimated lat-long using source gps,
    #  estimated distance from pin-hole geometry, and estimated bearing
    # TODO: measure estimated lat-long error, estimated distance error, estimated bearing error
    # TODO: plot all errors

    def find_allpossible_latlong_of_DD(self, bearing_of_DD_from_HD,
                                       distance_betw_HD_n_DD_x_side,
                                       distance_betw_HD_n_DD_y_side,
                                       HD_lat,
                                       HD_long):

        lat1, long1 = LatLonUtil.getPointByDistanceAndBearing(HD_lat, HD_long,
                                                              bearing_of_DD_from_HD,
                                                              distance_betw_HD_n_DD_x_side)

        lat2, long2 = LatLonUtil.getPointByDistanceAndBearing(HD_lat, HD_long,
                                                              bearing_of_DD_from_HD,
                                                              distance_betw_HD_n_DD_y_side)

        # print("possible lat-long pairs: ")
        # print(lat1, long1, lat2, long2)
        return lat1, long1, lat2, long2

    @staticmethod
    def find_distance_betw_GT_GPS_to_estimated(DD_GT_lat, DD_GT_long,
                                                DD_estimated_lat_x, DD_estimated_long_x,
                                                DD_estimated_lat_y, DD_estimated_long_y):

        dist_for_x = LatLonUtil.dist(DD_GT_lat, DD_GT_long, DD_estimated_lat_x, DD_estimated_long_x)
        dist_for_y = LatLonUtil.dist(DD_GT_lat, DD_GT_long, DD_estimated_lat_y, DD_estimated_long_y)

        print("all distances: ")
        print(dist_for_x, dist_for_y)
        return dist_for_x, dist_for_y

    def estimate_location(self, local_path, filename):
        df = pd.read_csv((local_path + '/' + filename), float_precision='round_trip')

        distances = [self.find_distance(xmin, ymin, xmax, ymax)
                     for xmin, ymin, xmax, ymax in
                     zip(df['xmin'], df['ymin'], df['xmax'], df['ymax'])]

        df[['est_dist_x', 'est_dist_y']] = pd.DataFrame(distances)

        angles = [self.find_angle(xmin, ymin + ((ymax - ymin) / 2), xmax, ymin + ((ymax - ymin) / 2))
                  for xmin, ymin, xmax, ymax in
                  zip(df['xmin'], df['ymin'], df['xmax'], df['ymax'])]

        df[['est_angle_btwn_boun_box_cam']] = pd.DataFrame(angles)

        bearings = [self.find_bearing(xmin, ymin, xmax, ymax)
                    for xmin, ymin, xmax, ymax in
                    zip(df['xmin'], df['ymin'], df['xmax'], df['ymax'])]

        df[['est_bearing_btwn_cam_obj']] = pd.DataFrame(bearings)

        allpossible_latlong = [self.find_allpossible_latlong_of_DD(bearing_of_DD_from_HD,
                                                                           distance_betw_HD_n_DD_x_side,
                                                                           distance_betw_HD_n_DD_y_side,
                                                                           HD_lat,
                                                                           HD_long)
                               for (bearing_of_DD_from_HD,
                                    distance_betw_HD_n_DD_x_side,
                                    distance_betw_HD_n_DD_y_side,
                                    HD_lat,
                                    HD_long) in
                               zip(df['est_bearing_btwn_cam_obj'], df['est_dist_x'],
                                   df['est_dist_y'], df['HD_GT_lat'], df['HD_GT_long'])]

        df[['lat_x', 'long_x', 'lat_y', 'long_y']] = pd.DataFrame(allpossible_latlong)

        # all_dists = [self.find_distance_betw_GT_GPS_to_estimated(DD_GT_lat, DD_GT_long,
        #                                                                  DD_estimated_lat_x, DD_estimated_long_x,
        #                                                                  DD_estimated_lat_y, DD_estimated_long_y)
        #              for (DD_GT_lat, DD_GT_long,
        #                   DD_estimated_lat_x, DD_estimated_long_x,
        #                   DD_estimated_lat_y, DD_estimated_long_y) in
        #              zip(df['GPS_Latitude'], df['GPS_Longitude'],
        #                  df['lat_x'], df['long_x'], df['lat_y'], df['long_y'])]
        #
        # df[['dist_diff_betw_est_x_and_GT_GPS', 'dist_diff_betw_est_y_and_GT_GPS']] = pd.DataFrame(all_dists)

        df.to_csv(local_path + '/' + ('camera_localization_' + filename), index=False)

        return len(df.index)
def __main():
    cam_localize = CameraBasedLocalization()

    local_path = "data\experiment1"

    filename = "merged_test1-export.csv"
    tic = time.perf_counter()
    num_of_records = cam_localize.estimate_location(local_path, filename)
    toc = time.perf_counter()
    execution_time_per_record = (toc - tic) / num_of_records

    print("total_execution_time: ",(toc - tic))
    print("num_of_records: ", num_of_records)
    print("execution_time_per_record: ",execution_time_per_record)

    filename = "merged_test2-export.csv"
    tic = time.perf_counter()
    num_of_records = cam_localize.estimate_location(local_path, filename)
    toc = time.perf_counter()

    execution_time_per_record = (toc - tic) / num_of_records

    print("total_execution_time: ", (toc - tic))
    print("num_of_records: ", num_of_records)
    print("execution_time_per_record: ", execution_time_per_record)

    tic = time.perf_counter()
    filename = "merged_test3-export.csv"
    num_of_records = cam_localize.estimate_location(local_path, filename)
    toc = time.perf_counter()

    execution_time_per_record = (toc - tic) / num_of_records

    print("total_execution_time: ", (toc - tic))
    print("num_of_records: ", num_of_records)
    print("execution_time_per_record: ", execution_time_per_record)
if __name__ == '__main__':
    __main()