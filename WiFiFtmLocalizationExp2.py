from LatLonUtil import *
import pandas as pd


class WiFiFtmLocalizationExp2:
    def __init__(self, experiment_id, local_path):
        self.experiment_id = experiment_id
        self.local_path = local_path

    @staticmethod
    def find_allpossible_latlong_of_DD(bearing_of_DD_from_HD_oneside,
                                       bearing_of_DD_from_HD_otherside,
                                       distance_betw_HD_n_DD,
                                       HD_lat,
                                       HD_long):

        lat1, long1 = LatLonUtil.getPointByDistanceAndBearing(HD_lat, HD_long,
                                                              bearing_of_DD_from_HD_oneside,
                                                              distance_betw_HD_n_DD)

        lat2, long2 = LatLonUtil.getPointByDistanceAndBearing(HD_lat, HD_long,
                                                              bearing_of_DD_from_HD_otherside,
                                                              distance_betw_HD_n_DD)

        # print("possible lat-long pairs: ")
        # print(lat1, long1, lat2, long2)
        return lat1, long1, lat2, long2

    def estimate_DD_location(self, triangle_height):
        df = pd.read_csv(
            self.local_path + '/' + ('merged_' + str(triangle_height) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')

        # Step 1: find the bearing between HD to pseudoHD
        bearings_betw_HD_pseudoHD = [LatLonUtil.get_bearing(HD_lat, HD_long, pseudoHD_lat, pseudoHD_long)
                                     for (HD_lat, HD_long, pseudoHD_lat, pseudoHD_long) in
                                     zip(df['HD_GT_lat'], df['HD_GT_long'], df['pseudoHD_GT_lat'],
                                         df['pseudoHD_GT_long'])]

        df['estimated_bearing_HD_pseudoHD'] = bearings_betw_HD_pseudoHD

        # Step 2: find all possible bearings of DD w.r.t. HD in relative to the vector of HD->pseudoHD
        possible_bearings_of_DD_from_HD = \
            [LatLonUtil.find_all_possible_bearings_of_DD(bearing_betw_HD_pseudoHD, angle_between_pseudoHD_DD)
             for (bearing_betw_HD_pseudoHD, angle_between_pseudoHD_DD) in
             zip(df['estimated_bearing_HD_pseudoHD'], df['angle_HD'])]

        df[['bearing_of_DD_from_HD_oneside', 'bearing_of_DD_from_HD_otherside']] \
            = pd.DataFrame(possible_bearings_of_DD_from_HD)

        # Step 3: find a corresponding lat-long point for DD's each possible bearing derived in Step 2.
        allpossible_latlong = [self.find_allpossible_latlong_of_DD(bearing_of_DD_from_HD_oneside,
                                                                         bearing_of_DD_from_HD_otherside,
                                                                         distance_betw_HD_n_DD,
                                                                         HD_lat,
                                                                         HD_long)
                               for (bearing_of_DD_from_HD_oneside,
                                    bearing_of_DD_from_HD_otherside,
                                    distance_betw_HD_n_DD,
                                    HD_lat,
                                    HD_long) in
                               zip(df['bearing_of_DD_from_HD_oneside'], df['bearing_of_DD_from_HD_otherside'],
                                   df['side_HD_DD'], df['HD_GT_lat'], df['HD_GT_long'])]

        df[['lat1', 'long1', 'lat2', 'long2']] = pd.DataFrame(allpossible_latlong)

        # Step 4: find the closest possible latlong among all the possible latlong of DD
        #  w.r.t DD's previous latlong. declare the closest possible latlong of DD's
        #  previous latlong as DD's latest latlong

        DD_latest_latlong_with_est_bearing = [LatLonUtil.find_closest_latlong_to_prev_location(
            DD_prev_lat, DD_prev_long,
            lat1, long1, lat2, long2, bearing_oneside, bearing_otherside)
            for (DD_prev_lat, DD_prev_long,
                 lat1, long1, lat2, long2, bearing_oneside, bearing_otherside) in
            zip(df['DD_prev_lat'], df['DD_prev_long'],
                df['lat1'], df['long1'], df['lat2'], df['long2'],
                df['bearing_of_DD_from_HD_oneside'], df['bearing_of_DD_from_HD_otherside'])]

        df[['DD_latest_lat', 'DD_latest_long', 'HD_DD_est_bearing']] = pd.DataFrame(DD_latest_latlong_with_est_bearing)

        df.to_csv(self.local_path + '/' + ('final_' + str(triangle_height) + '_m_wifi_' + self.experiment_id + '.csv'),
                  index=False)

# def __main():
#
#     local_path = "data/experiment2"
#     experiment_id = 'exp2'
#     wifi_localize = WiFiFtmBasedLocalization(local_path, experiment_id)
#
#     triangle_height = 3
#     wifi_localize.estimate_DD_location(triangle_height)
#
#     triangle_height = 5
#     wifi_localize.estimate_DD_location(triangle_height)
#
#     triangle_height = 7
#     wifi_localize.estimate_DD_location(triangle_height)
#
#     triangle_height = 10
#     wifi_localize.estimate_DD_location(triangle_height)
#
#     triangle_height = 15
#     wifi_localize.estimate_DD_location(triangle_height)
#
# if __name__ == '__main__':
#     __main()
