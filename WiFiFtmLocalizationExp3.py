from LatLonUtil import *
import pandas as pd
import numpy as np
import math


class WiFiFtmLocalizationExp3:
    def __init__(self, experiment_id, local_path):
        self.experiment_id = experiment_id
        self.local_path = local_path

    @staticmethod
    def find_diff(estimated, GT_value):
        return round(abs(estimated - GT_value), 3)

    @staticmethod
    def find_theta(opposite, hypotenuse):
        theta = None
        if opposite and hypotenuse:
            try:
                theta = round(math.degrees(math.asin(opposite / hypotenuse)), 1)
            except ValueError as e:
                print(e)
                print("opposite", opposite)
                print("hypotenuse", hypotenuse)
            return theta

    def bearing_relative_to_four_directions(self, theta):
        bearings = dict.fromkeys(range(4))
        if theta:
            bearings[0] = theta
            bearings[1] = 180 - theta
            bearings[2] = 180 + theta
            bearings[3] = 360 - theta
        return bearings

    def find_allpossible_latlong_of_DD_with_bearing(self, source_lat,
                                                    source_long,
                                                    theta,
                                                    distance_betw_src_dest):

        bearings = self.bearing_relative_to_four_directions(theta)
        allpossible_latlongs = dict.fromkeys(range(len(bearings)))

        for i in range(len(bearings)):
            if bearings[i]:
                lat, long = LatLonUtil.getPointByDistanceAndBearing(source_lat,
                                                                    source_long,
                                                                    bearings[i],
                                                                    distance_betw_src_dest)

                allpossible_latlongs[i] = lat, long, bearings[i]
        return allpossible_latlongs

    def find_closest_latlong_to_prev_location(self, DD_prev_lat, DD_prev_long,
                                              latlongs_in_four_diretions):

        nearest_latlong = ('Invalid', 'Invalid', 'Invalid')
        if latlongs_in_four_diretions:
            closest_dist = 999999
            for i in range(len(latlongs_in_four_diretions)):
                if latlongs_in_four_diretions[i]:
                    dist = LatLonUtil.dist(DD_prev_lat, DD_prev_long,
                                       latlongs_in_four_diretions[i][0],
                                       latlongs_in_four_diretions[i][1])

                    if dist < closest_dist:
                        closest_dist = dist
                        nearest_latlong = latlongs_in_four_diretions[i]

        return nearest_latlong

    def estimate_DD_location(self, triangle_height):

        # NOTE: In experiment 3 the DD carries the AP and the HD carries 2 WiFiFtm devices (pixel 2 phones)
        # In the sourcecode implementation we keep the naming-tradition same, that is, 2 WiFiFtm devices
        # are randomly called pseudoHD and DD. and the AP is called HD,
        # However, the setup can be vice versa, that the DD carries two WiFiFtm devices and the HD carries the AP.

        # Step 1: find the difference in distance between measured HD_pseudoHD and HD_DD,
        # this difference is the opposite of the right-triangle created by the phase shift of adjacent signals.
        # In this setup the distance sides are assumed to be the signals.

        df = pd.read_csv(
            self.local_path + '/' + ('merged_' + str(triangle_height) + '_m_wifi_' + self.experiment_id + '.csv'),
            float_precision='round_trip')

        opposites = [self.find_diff(estimated_dist, GT_dist)
                     for (estimated_dist, GT_dist) in
                     zip(df['side_HD_DD'], df['side_HD_pseudoHD'])]

        df[['opposites']] = pd.DataFrame(opposites)

        # Step 2: the Hypotenuse is the fixed distance between pseudoHD-DD. With given opposite and hypotenuse
        # we find the theta of the right triangle. This theta is the incident angle for both the incident signals.

        hypotenuses = df['GT_side_pseudoHD_DD']

        thetas = [self.find_theta(opposite, hypotenuse)
                  for (opposite, hypotenuse) in
                  zip(opposites, hypotenuses)]

        print(thetas)
        df[['thetas']] = pd.DataFrame(thetas)

        # Step 3: Based on the theta, there is a possibility of the DD located on any one cardinal directions
        # among the four; North, South, East, West. So, we derive 4 possible bearing angles
        # relative to each direction. Derive four different set of lat-long pairs
        # corresponding to four different bearings.
        allpossible_latlong = [self.find_allpossible_latlong_of_DD_with_bearing(source_lat,
                                                                                source_long,
                                                                                theta,
                                                                                distance_betw_src_dest)
                               for (source_lat,
                                    source_long,
                                    theta,
                                    distance_betw_src_dest) in
                               zip(df['HD_GT_lat'], df['HD_GT_long'],
                                   thetas, df['side_HD_DD'])]

        print(allpossible_latlong)

        # Step 4: Rule out 3 lat-long pairs among the 4 pairs comparing nearest proximity with
        # DD's previous lat-long position.

        DD_latest_latlong_with_est_bearing = [self.find_closest_latlong_to_prev_location(
                                                DD_prev_lat, DD_prev_long,
                                                latlongs_in_four_diretions)
                                                for (DD_prev_lat, DD_prev_long,
                                                     latlongs_in_four_diretions) in
                                                zip(df['DD_prev_lat'], df['DD_prev_long'],
                                                    allpossible_latlong)]

        df[['DD_latest_lat', 'DD_latest_long', 'HD_DD_est_bearing']] = pd.DataFrame(DD_latest_latlong_with_est_bearing)

        df.to_csv(self.local_path + '/' + ('final_' + str(triangle_height) + '_m_wifi_' + self.experiment_id + '.csv'),
                  index=False)

def __main():
    print()

if __name__ == '__main__':
    __main()
