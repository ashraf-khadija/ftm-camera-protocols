# TODO: Run this file to execute Experiment 3. Includes, data processing, location estimation, evaluation, graph plotting

from ProcessWiFiFtmData import *
from WiFiFtmLocalizationExp3 import *
from EvaluationWiFiFtmLocalization import *


class Experiment3(ProcessWiFiFtmData, WiFiFtmLocalizationExp3, EvaluationWiFiFtmLocalization):
    def __init__(self, experiment_id, raw_data_path, local_path):
        ProcessWiFiFtmData.__init__(self, experiment_id, raw_data_path, local_path)
        WiFiFtmLocalizationExp3.__init__(self, experiment_id, local_path)
        EvaluationWiFiFtmLocalization.__init__(self, experiment_id, local_path)

    def process_wifi_data(self, triangle_height):
        merged_df = self.map_field_data(triangle_height)

        self.load_GTdata(merged_df, triangle_height)

        # self.measure_sss_angles(merged_df)

        print(merged_df)
        merged_df.to_csv(
            self.local_path + '/' + ('merged_' + str(triangle_height) + '_m_wifi_' + self.experiment_id + '.csv'),
            index=False)

def __main():
    experiment_id = 'exp3'
    local_path = "data/experiment3"
    raw_data_path = 'C:\khadija.csedu\MCPS\summer 2021\experiment-data\wifi-ftm-data\experiment3-HD-DD'

    exp3 = Experiment3(experiment_id, raw_data_path, local_path)

    triangle_height = 3
    exp3.process_wifi_data(triangle_height)
    exp3.estimate_DD_location(triangle_height)
    exp3.accuracy_measurement(triangle_height)

    triangle_height = 5
    exp3.process_wifi_data(triangle_height)
    exp3.estimate_DD_location(triangle_height)
    exp3.accuracy_measurement(triangle_height)

    triangle_height = 7
    exp3.process_wifi_data(triangle_height)
    exp3.estimate_DD_location(triangle_height)
    exp3.accuracy_measurement(triangle_height)

    triangle_height = 10
    exp3.process_wifi_data(triangle_height)
    exp3.estimate_DD_location(triangle_height)
    exp3.accuracy_measurement(triangle_height)

    triangle_height = 15
    exp3.process_wifi_data(triangle_height)
    exp3.estimate_DD_location(triangle_height)
    exp3.accuracy_measurement(triangle_height)


if __name__ == '__main__':
    __main()
