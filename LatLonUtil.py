import math
from WiFiFtmLocalizationExp2 import *
import numpy
from math import radians, cos, sin, asin, sqrt
from PropertyReadWriteUtils import *

# Bearing and distance calculating tool in google maps:
# https://www.acscdg.com/
# Latitude/Longitude Distance Calculator
# https://www.meridianoutpost.com/resources/etools/calculators/calculator-latitude-longitude-distance.php?

class LatLonUtil:
    def __init__(self):
        self.srcLat = None
        self.srcLon = None
        self.bearing = None             # in degrees
        self.distanceBetweenM = None    # miles

    def load(self, srcLat, srcLon, bearing, distanceBetweenM):
        self.srcLat = srcLat
        self.srcLon = srcLon
        self.bearing = bearing
        self.distanceBetweenM = distanceBetweenM

    @staticmethod
    def parse_lat_long(latlong):
        lat = None
        long = None
        if latlong is not None:
            it = iter(latlong)
            try:
                lat = float(next(it))
                long = float(next(it))
            except StopIteration as e:
                print(e)
        return lat, long

    @staticmethod
    def getPointByDistanceAndBearing(srcLat, srcLon, bearing, distanceBetweenM):
        earthRadius = float(PropertyReadWriteUtils.read_value_by_key("EARTH_RADIUS_IN_METERS"))

        bearingR = math.radians(bearing)

        latR = math.radians(srcLat)
        lonR = math.radians(srcLon)

        distanceToRadius = distanceBetweenM / earthRadius

        newLatR = math.asin(math.sin(latR) * math.cos(distanceToRadius) +
                math.cos(latR) * math.sin(distanceToRadius) * math.cos(bearingR))
        newLonR = lonR + math.atan2(math.sin(bearingR) * math.sin(distanceToRadius) * math.cos(latR),
                math.cos(distanceToRadius) - math.sin(latR) * math.sin(newLatR))

        latNew = round(math.degrees(newLatR), 6)
        lonNew = round(math.degrees(newLonR), 6)

        return latNew, lonNew

    # https://stackoverflow.com/questions/54873868/python-calculate-bearing-between-two-lat-long
    @staticmethod
    def get_bearing(lat1, long1, lat2, long2):
        dLon = (long2 - long1)
        x = math.cos(math.radians(lat2)) * math.sin(math.radians(dLon))
        y = math.cos(math.radians(lat1)) * math.sin(math.radians(lat2)) \
                - math.sin(math.radians(lat1)) * math.cos(math.radians(lat2)) * math.cos(math.radians(dLon))
        brng = numpy.arctan2(x, y)
        brng = round(numpy.degrees(brng))

        if brng < 0:
            return brng + 360
        return brng

    @staticmethod
    def find_all_possible_bearings_of_DD(bearing_betw_HD_pseudoHD, angle_between_pseudoHD_DD):
        bearing_of_DD_from_HD_oneside = (bearing_betw_HD_pseudoHD - angle_between_pseudoHD_DD)
        if bearing_of_DD_from_HD_oneside < 0:
            bearing_of_DD_from_HD_oneside = bearing_of_DD_from_HD_oneside + 360

        bearing_of_DD_from_HD_otherside = (bearing_betw_HD_pseudoHD + angle_between_pseudoHD_DD) % 360

        return bearing_of_DD_from_HD_oneside, bearing_of_DD_from_HD_otherside


    @staticmethod
    def find_closest_latlong_to_prev_location(DD_prev_lat, DD_prev_long,
                                              lat1, long1, lat2, long2,
                                              bearing_oneside, bearing_otherside):

        possible_dist1 = LatLonUtil.dist(DD_prev_lat, DD_prev_long, lat1, long1)
        possible_dist2 = LatLonUtil.dist(DD_prev_lat, DD_prev_long, lat2, long2)

        print("all distances: ")
        print(possible_dist1, possible_dist2)
        if possible_dist1 < possible_dist2:
            DD_latest_lat, DD_latest_long = lat1, long1
            return DD_latest_lat, DD_latest_long, bearing_oneside
        else:
            DD_latest_lat, DD_latest_long = lat2, long2
            return DD_latest_lat, DD_latest_long, bearing_otherside


    @staticmethod
    def is_number_string(num):
        try:
            float(num)
            return True
        except:
            return False

    @staticmethod
    def is_real_number(num):
        if type(num) == int or type(num) == float:
            return True
        else:
            return False

    # https://medium.com/analytics-vidhya/finding-nearest-pair-
    # of-latitude-and-longitude-match-using-python-ce50d62af546
    @staticmethod
    def dist(lat1_arg, long1_arg, lat2_arg, long2_arg):
        distanceMeter = - 1
        if lat1_arg is None or long1_arg is None or lat2_arg is None or long2_arg is None:
            return -1
        if type(lat1_arg) == str and not LatLonUtil.is_number_string(lat1_arg):
            return -1
        if type(long1_arg) == str and not LatLonUtil.is_number_string(long1_arg):
            return -1
        if type(lat2_arg) == str and not LatLonUtil.is_number_string(lat2_arg):
            return -1
        if type(long2_arg) == str and not LatLonUtil.is_number_string(long2_arg):
            return -1

        lat1 = float(lat1_arg)
        long1 = float(long1_arg)
        lat2 = float(lat2_arg)
        long2 = float(long2_arg)

        # convert decimal degrees to radians
        lat1, long1, lat2, long2 = map(radians, [lat1, long1, lat2, long2])
        # haversine formula
        dlon = long2 - long1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * asin(sqrt(a))
        # Radius of earth in meters is 6371000
        distanceMeter = round(float(PropertyReadWriteUtils.read_value_by_key("EARTH_RADIUS_IN_METERS")) * c, 1)
        return distanceMeter

def __main():
    print()

if __name__ == '__main__':
    __main()
